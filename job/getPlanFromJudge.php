<?php
/**
 *
 */
include_once('inc/common.php') ;


$SQLCmd = "SELECT idx, plan_name FROM tb_plan_list" ;
$retArr = $objDB->ExecuteSQL( $SQLCmd) ;

$planArr = array() ;
foreach ($retArr as $iCnt => $row) {
	$planArr[$row['idx']] = $row['plan_name'] ;
}

$SQLCmd = "SELECT * FROM tb_plan_answer WHERE idx IN (
			SELECT max(idx) aid FROM tb_plan_answer GROUP BY pid) AND pid IS NOT NULL" ;
$retArr = $objDB->ExecuteSQL( $SQLCmd) ;

    // [item1] => 3625m
    // [item2] => 自行車道停車場、引導標線
    // [item4] => 古坑綠色隧道景觀公園、福祿壽酒廠、228紀念公園、古坑河濱運動公園  、蜜蜂故事館 、古坑綠色隧道假日市集、古坑緑色隧道89農場
    // [item5] => 有 ex：蜜蜂故事館、古坑綠色隧道交流驛站228紀念公園
    // [item6] => 兩旁種植著約五十年歷史的芒果樹，綠蔭蔽天形成美麗的綠色隧道，沿路台糖土地種植甘蔗田
    // [item7] => 古坑綠色隧道景觀公園、綠色隧道大草皮、蜜蜂故事館
    // [item8] => 綠色隧道公園生態池於3年前闢建，由鄉公所委託農委會特有生物中心規畫，池內種植水蠟燭、浮萍等多種水生植物，還有枯木、石頭供鳥類、蛙類等生物停棲，營造一個小型的生態體系。
    // [item9] => 否
    // [item10] => 行人及自行車道專用道
    // [item11] => 綠色隧道站(臺灣好行)、福祿壽酒廠站、新興站
    // [item12] => 古坑綠色隧道景觀公園、福祿壽酒廠
    // [item13] => 古坑綠色隧道景觀公園、福祿壽酒廠、228紀念公園、古坑河濱運動公園  、蜜蜂故事館 、古坑綠色隧道假日市集、古坑緑色隧道89農場
    // [item14] => 蜜蜂故事館 、古坑綠色隧道假日市集(廖媽媽綠色隧道杏鮑菇、古坑綠色隧道貝菈手工鮮果冰棒、昕素食滷味....)、Soka綠色隧道-古坑咖啡教學-精品咖啡、廳福祿壽酒廠
    // [item15] => 否

foreach ($retArr as $iCnt => $row) {
	echo $row['pid']."\n" ;
	$wArr = array() ;
	$tmpStr = json_decode( urldecode( $row['answer']), true) ;

	if ( !is_array( $tmpStr)) {
		echo " {$row['pid']} 有問題\n ";
		$tmpStr = json_decode( $row['answer'], true) ;
		if ( !is_array( $tmpStr)) {
			echo " {$row['pid']} 還是有問題\n ";
			print_r( $row['answer']);
		}
	}
	// if ( $row['pid'] == '2') {
		// echo "Angus" ;
		// print_r( $tmpStr) ;

	$wArr['pName'] = mb_convert_encoding( $planArr[$row['pid']],"big5", "utf-8") ;
	// print_r( $pName) ;
	$wArr['item1'] = mb_convert_encoding( $tmpStr['item1'],"big5", "utf-8") ;
	$wArr['startRoute'] = mb_convert_encoding( $row['startRoute'],"big5", "utf-8") ;
	$wArr['plan'] = mb_convert_encoding( $row['plan'],"big5", "utf-8") ;

	$wArr['planDate1'] = mb_convert_encoding( $row['planDate1'],"big5", "utf-8") ;
	$wArr['planDate2'] = mb_convert_encoding( $row['planDate2'],"big5", "utf-8") ;
	$wArr['planDate3'] = mb_convert_encoding( $row['planDate3'],"big5", "utf-8") ;
	$wArr['planDate4'] = mb_convert_encoding( $row['planDate4'],"big5", "utf-8") ;

	$wArr['environment'] = mb_convert_encoding( $row['environment'],"big5", "utf-8") ;
	$wArr['route'] = mb_convert_encoding( $row['route'],"big5", "utf-8") ;

	$wArr['item2'] = mb_convert_encoding( $tmpStr['item2'],"big5", "utf-8") ;
	$wArr['item4'] = mb_convert_encoding( $tmpStr['item4'],"big5", "utf-8") ;
	$wArr['item5'] = mb_convert_encoding( $tmpStr['item5'],"big5", "utf-8") ;
	$wArr['item6'] = mb_convert_encoding( $tmpStr['item6'],"big5", "utf-8") ;
	$wArr['item7'] = mb_convert_encoding( $tmpStr['item7'],"big5", "utf-8") ;
	$wArr['item8'] = mb_convert_encoding( $tmpStr['item8'],"big5", "utf-8") ;
	$wArr['item9'] = mb_convert_encoding( $tmpStr['item9'],"big5", "utf-8") ;
	$wArr['item10'] = mb_convert_encoding( $tmpStr['item10'],"big5", "utf-8") ;
	$wArr['item11'] = mb_convert_encoding( $tmpStr['item11'],"big5", "utf-8") ;
	$wArr['item12'] = mb_convert_encoding( $tmpStr['item12'],"big5", "utf-8") ;
	$wArr['item13'] = mb_convert_encoding( $tmpStr['item13'],"big5", "utf-8") ;
	$wArr['item14'] = mb_convert_encoding( $tmpStr['item14'],"big5", "utf-8") ;
	$wArr['item15'] = mb_convert_encoding( $tmpStr['item15'],"big5", "utf-8") ;

	$outputFileName = "{$row['pid']}附件二：自行車道自我評量表.doc" ;
	$wordStr = file_get_contents("00附件二：自行車道自我評量表.doc") ;
	// print_r( $wordStr) ;
	// $wordStr = "Angus %s 1234" ;
	// $outputStr = sprintf( $wordStr, $pName) ;
	$outputStr = sprintf( $wordStr, $wArr['pName'],$wArr['item1'],$wArr['startRoute'],$wArr['plan'],$wArr['planDate1'],$wArr['planDate2'],$wArr['planDate3'],$wArr['planDate4'],$wArr['environment'],$wArr['route'],$wArr['item2'],$wArr['item4'],$wArr['item5'],$wArr['item6'],$wArr['item7'],$wArr['item8'],$wArr['item9'],$wArr['item10'],$wArr['item11'],$wArr['item12'],$wArr['item13'],$wArr['item14'],$wArr['item15'],"Angus","Angus") ;


	$fp = fopen( $outputFileName , 'w' ) ;
	fwrite($fp, $outputStr) ;
	fclose($fp);
	// }
	// exit();
}

