<?
// http://www.php.net/manual/en/mysql.examples-basic.php
class MysqlConnClass {
	// Base variables
	var $lastError ;				// Holds the last error 執行的錯誤訊息
	var $lastQuery ;				// Holds the last query 執行語法
	var $result ;					// Holds the MySQL query result 執行結果
	var $records ;					// Holds the total number of records returned 回傳筆數
	var $affected ;					// Holds the total number of records affected
	var $rawResults ;				// Holds raw 'arrayed' results
	var $arrayedResult ;			// Holds an array of the result 取回多筆資料 以陣列呈現 取回單筆資料

	var $_dbLink ;					// Database Connection Link

	function __construct( $persistant = false ) {

		// define("cDBhost"     ,":/Applications/MAMP/tmp/mysql/mysql.sock");  //    資料庫主機
		// define("cDBhost"     ,"localhost");  //    資料庫主機
		// define("cDBuname"    ,"root");        	//    使用者名稱
		// define("cDBpass"     ,"root");      	//    使用者密碼
		// define("cDBname"     ,"933");        //    資料庫名稱

		// define("cDBhost"     ,"anguskh.com:35678");  //    資料庫主機
		// define("cDBhost"     ,"192.168.1.100");  //    資料庫主機
		//
		// define("cDBhost"     ,"192.168.1.235");	//    資料庫主機
		// define("cDBuname"    ,"oasuser");		//    使用者名稱
		// define("cDBpass"     ,"oas123");		//    使用者密碼
		// define("cDBname"     ,"oas");			//    資料庫名稱

		define("cDBhost"     ,"127.0.0.1");	//    資料庫主機
		define("cDBuname"    ,"eventpal");		//    使用者名稱
		define("cDBpass"     ,"eventpal123");		//    使用者密碼
		define("cDBname"     ,"eventpal_bike");			//    資料庫名稱

		$this->CloseConnection() ;

		$this->_dbLink = mysqli_connect( cDBhost, cDBuname, cDBpass, cDBname) or die('Error '. mysqli_connect_error()) ;
		// mysql_select_db( cDBname, $this->_dbLink ) or die('Could not select database.') ;
		mysqli_query( $this->_dbLink, "SET CHARACTER SET UTF8") ;
	}

	function __destruct() {
		$this->CloseConnection() ;
	}

	/* ******************
	 * Public Functions *
	 * ******************/
	public function getResult($tabName = "", $colArr = "", $orderby = "")
	{
		if( !empty( $tabName)) {
			$whereStr = "" ;
			$orderbyStr = "" ;
			if ( is_array($colArr) && count( $colArr) >= 1) {
				foreach ($colArr as $key => $value) {
					$whereStr .= "AND {$key}='{$value}' ";
				}
			}

			if ( is_array($orderby) && count( $orderby) >= 1) {
				$orderbyStr = "order by " ;
				foreach ($orderby as $key => $value) {
					$orderbyStr .= " {$key} {$value},";
				}
				$orderbyStr = substr($orderbyStr, 0, strlen($orderbyStr)-1) ;
			}

			$SQLCmd = "SELECT * FROM {$tabName} WHERE 1 = 1 {$whereStr} {$orderbyStr} limit 100" ;
			// echo $SQLCmd ;

			return $this->ExecuteSQL($SQLCmd) ;
		} else {
			return false ;
		}
	 }

	public function insert( $tabName = "", $args = array() ) {
		if( !empty( $tabName)) {
			$colStr = "";
			$dataStr = "";
			foreach ($args[0] as $key=>$val) {
				$colStr 	.= "{$key},";
				$dataStr	.= $this->makeInsertStr(isset( $args[1][$key]) ? $args[1][$key] : "", $val);
			}
			$colStr  = substr($colStr,  0, strlen($colStr)-1);
			$dataStr = substr($dataStr, 0, strlen($dataStr)-1);


			$SQLCmd = "INSERT INTO {$tabName} ($colStr) VALUES ({$dataStr})" ;
			// echo "{$SQLCmd}<br>";
			mysqli_query( $this->_dbLink, $SQLCmd) ;
			return TRUE ;
		} else {
			return FALSE ;
		}
	}

	function QuerySQL( $SQLCmd) {
		$this->lastQuery 	= $SQLCmd ;
		if( $this->result 	= mysqli_query( $this->_dbLink, $SQLCmd) ) {
			return TRUE ;
		} else {
			$this->lastError = mysqli_error( $this->_dbLink )."\n" ;
			return FALSE ;
		}
	}

	/**
	 * Executes MySQL query
	 */
	function ExecuteSQL( $SQLCmd ) {
		$this->lastQuery 	= $SQLCmd ;
		if( $this->result 	= mysqli_query( $this->_dbLink, $SQLCmd) ) {

			if ( strpos(" ".strtolower( $SQLCmd), "select") > 0) {
				$this->records 	= mysqli_num_rows( $this->result);
			} else {
				$this->affected	= mysqli_affected_rows( $this->_dbLink );
				$this->records = 0 ;
			}

			if ( $this->records == 1 ) {
				$this->ArrayResults() ;
				return $this->arrayedResult ;
			} else if( $this->records > 1 ) {
				$this->ArrayResults() ;
				return $this->arrayedResult ;
			}else{
				return true ;
			}

		} else {
			$this->lastError = mysqli_error( $this->_dbLink )."\n" ;
			return false ;
		}
	}


	/**
	 * 'Arrays' a single result
	 */
	function ArrayResult() {
		$this->arrayedResult = mysqli_fetch_assoc($this->result) or die ( mysqli_connect_error() ) ;
		return $this->arrayedResult;
	}

	/**
	 * 'Arrays' multiple result
	 * 取出資料 以陣列產出
	 */
	function ArrayResults() {
		// echo "this->records : {$this->records}\n";
		if($this->records == 1){
			return $this->ArrayResult() ;
		}

		$this->arrayedResult = array();
		while ($data = mysqli_fetch_assoc($this->result)) {
			array_push( $this->arrayedResult, $data ) ;
			// $this->arrayedResult[] = $data;
		}
		return $this->arrayedResult;
	}

	// Returns last insert ID
	function LastInsertID(){
		// return mysql_insert_id();
		 return $this->_dbLink->insert_id;
	}

	// Closes the connections
	function CloseConnection(){
		if( $this->_dbLink) {
			mysqli_close( $this->_dbLink) ;
		}
	}


	function makeInsertStr ($type, $val) {
		$tmpStr = "";
		switch ($type) {
			default:
			case "string":
				$tmpStr = "'{$val}',";
				break;
			case "integer":
				if (trim($val)) {
					$tmpStr = "$val,";
				} else {
					$tmpStr = "'',";
				}
				break;
			case "datetime":
				if ($val == "now()") {
					$tmpStr = "$val,";
				} else {
					$tmpStr = "'$val',";
				}
				break;
		}

		return $tmpStr;
	}

	function records ( $SQLCmd ) {
		$this->lastQuery 	= $SQLCmd ;
		if( $this->result 	= mysqli_query( $this->_dbLink, $SQLCmd) ) {
			if ( strpos(" ".strtolower( $SQLCmd), "select") > 0) {
				$this->records 	= mysqli_num_rows( $this->result);
				return $this->records ;
			} else {
				$this->affected	= mysqli_affected_rows( $this->_dbLink );
				$this->records = 0 ;
			}
		} else {
			$this->lastError = mysqli_error( $this->_dbLink )."\n" ;
			return false ;
		}
	}

}

?>