<?/**
 * 基本設定
 */
error_reporting(E_ALL) ;
ini_set('display_errors','On') ;
date_default_timezone_set("Asia/Taipei"); //設定台北時間
session_start() ;

include_once ("mysql.connect.class.php") ;

$objDB = new MysqlConnClass() ;	//
//$objAtt = new AttributeSet() ;	//




function pre($val, $className="", $fileName="", $Parameters = "")
{
	echo '<pre>';
	echo trim($fileName)	? "file Name : {$fileName}\n"		: "" ;
	echo trim($className)	? "class Name : {$className}\n"		: "" ;
	echo trim($Parameters)	? "Parameters : {$Parameters}\n"	: "" ;
	print_r( $val);
	// var_dump( $val);
	echo '</pre>';
}


function alert($msg) {
echo <<<SCRIPT
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script Language="JavaScript">
  alert("$msg");
</script>
SCRIPT
;
}


function alert_redirect($url, $msg) {
echo <<<HTML
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<Script Language="JavaScript">
 	{
 		alert("$msg") ;
 		location.href="$url";
 	}
</SCRIPT>
HTML
;
}

function save_log( $msg, $fileName) {

	$folder = "log" ;
	if ( !is_dir( $folder)) {
		mkdir( $folder) ;
	}
	$fileDate = date( "Y_md" ) ;
	$logFileName = "{$folder}/{$fileName}_{$fileDate}.log" ;
	$fp = fopen($logFileName , 'a' ) ;
	$nowTime = date("Y/m/d H:i:s");

	if (is_array($msg)) {
		fwrite($fp,"{$nowTime} | ".print_r($msg, true)."\r\n") ;
		// fwrite($fp,print_r($msg, true)."\r\n") ;
	} else {
		fwrite($fp,"{$nowTime} | {$msg}\r\n") ;
		// fwrite($fp,"{$msg}\r\n") ;
	}
	 fclose($fp);
}
?>