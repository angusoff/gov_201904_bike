<?php
error_reporting(E_ALL) ;
ini_set('display_errors','On') ;
ini_set('display_startup_errors', 1);
date_default_timezone_set("Asia/Taipei"); //設定台北時間
// HTTP
define('SERVER_ADDRESS',	$_SERVER['HTTP_HOST']) ;
define('PROJECT_SITE',		'gov_201904_bike') ;

define('HTTP_SERVER',		'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/');

// HTTPS
define('HTTPS_SERVER',		'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/');

// DIR
define('DIR_INITIAL',		'/Applications/XAMPP/xamppfiles/htdocs/'.PROJECT_SITE) ;

define('DIR_APPLICATION',	DIR_INITIAL.'/bike/'); // 主要執行目錄

define('DIR_SYSTEM',		DIR_INITIAL.'/system/');
define('DIR_IMAGE',			DIR_INITIAL.'/image/');
define('DIR_LANGUAGE',		DIR_INITIAL.'/bike/language/');
define('DIR_TEMPLATE',		DIR_INITIAL.'/bike/view/template/');
define('DIR_CONFIG',		DIR_INITIAL.'/system/config/');
define('DIR_CACHE',			DIR_INITIAL.'/system/storage/cache/');
define('DIR_DOWNLOAD',		DIR_INITIAL.'/system/storage/download/');
define('DIR_LOGS',			DIR_INITIAL.'/system/storage/logs/');
define('DIR_MODIFICATION',	DIR_INITIAL.'/system/storage/modification/');
define('DIR_UPLOAD',		DIR_INITIAL.'/system/storage/upload/');

// DB
// define('DB_DRIVER',		'mysqli') ;
// define('DB_HOSTNAME',	'anguskh.com') ;
// define('DB_USERNAME',	'root') ;
// define('DB_PASSWORD',	'kuanhsin') ;
// define('DB_DATABASE',	'eventpal_bike') ;
// define('DB_PORT',		'3306') ;
// define('DB_PREFIX',		'oc_') ;

// define('DB_DRIVER',		'mysqli') ;
// define('DB_HOSTNAME',	'206.108.48.186') ;
// define('DB_USERNAME',	'eventpal') ;
// define('DB_PASSWORD',	'eventpal123') ;
// define('DB_DATABASE',	'eventpal_bike') ;
// define('DB_PORT',		'3306') ;
// define('DB_PREFIX',		'oc_') ;

define('DB_DRIVER',		'mysqli') ;
define('DB_HOSTNAME',	'localhost') ;
define('DB_USERNAME',	'root') ;
define('DB_PASSWORD',	'') ;
define('DB_DATABASE',	'eventpal_bike') ;
define('DB_PORT',		'3306') ;
define('DB_PREFIX',		'oc_') ;
