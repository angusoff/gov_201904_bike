<?php
class Csvtool{
    
    public function __construct() {
		
	}
    
    public function array2String($i_array, $i_blLine = false) {
        $quote = '"';
        $sep = ',';
        $lineLF = "\n";
        $lineCR = "\r";
        $lineCRLF = "\r\n";
        $NEWLINE = "\r\n";
        $stringCSV = '';
        $elements = array();
        if(count($i_array) > 0){
            foreach ($i_array as $data) {
                $data = str_replace($quote, '""', $data);
            
                //「,」「"」「\n」「\r」「\r\n」
                if (strpos($data, $sep) !== false || strpos($data, $quote) !== false
                        || strpos($data, $lineLF) !== false || strpos($data, $lineCR) !== false || strpos($data, $lineCRLF))
                    $elements[] = "{$quote}$data{$quote}";
                else
                    $elements[] = $data;
            }
        }
        if(count($elements) > 0){
            $stringCSV .= implode($sep, $elements);
        }
        if ($i_blLine) {
            $stringCSV .= $NEWLINE;
        }
        return $stringCSV;
    }
    
    public function array2CsvOnlyColumn($i_array, $i_columnNames) {
        $stCSV = "";
        if(count($i_array) > 0){
            foreach ($i_array as $var) {
                $columnData = array();
                foreach ($i_columnNames as $column) {
                    $columnData[] = $var[$column];
                }
                $stCSV .= array2String($columnData, true);
            }
        }
        return $stCSV;
    }
    
    public function check_encoding($tstring){
        $encode = mb_detect_encoding($tstring, array('UTF-8','big5','GBK','GB2312','ASCII'));
        if($encode != 'UTF-8'){
            $tstring = mb_convert_encoding($tstring,"UTF-8",$encode);
        }
        return $tstring;
    }
    
    public function _procDownloadText($fileName, $text) {
        header('Pragma: public');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: pre-check=0, post-check=0, max- age=0');
        header('Content-Transfer-Encoding: base64\n');
        header('Content-Type: application/octet-stream; name="' . $fileName . '"');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $text;
        exit();
    }
}
?>