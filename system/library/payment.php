<?php
class Payment{
    
    public $aryPayStatus = array();
    
    public $aryPayType = array();
    
    public function __construct() {
		$this->aryPayType["Credit"] = "信用卡";
        $this->aryPayType["WEBATM"] = "WEBATM";
        $this->aryPayType["ATM"]    = "ATM";
        $this->aryPayType["CVS"]    = "超商繳費";
        
        $this->aryPayStatus = array(1=>"未付款",2=>"已付款");
	}
    
}
?>