<?php
namespace Cart;
class Dis {
	private $source_id ;
	private $source_name ;
	private $account ;
	private $parent_id ;
	private $rateInfo ;
	private $bankInfo ;

	/**
	 * [__construct description]
	 * @param   [type]     $registry [description]
	 * @Another Angus
	 * @date    2019-02-13
	 */
	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		if (isset($this->session->data['dis_id'])) {
			$SQLCmd = "SELECT * FROM tb_resource WHERE idx=" . (int)$this->session->data['dis_id'] . " AND status = 1" ;
			$customer_query = $this->db->query( $SQLCmd) ;
			if ($customer_query->num_rows) {
				$this->session->data['dis_id'] = $customer_query->row['idx'] ;

				$this->source_id   = $customer_query->row['idx'] ;
				$this->source_name = $customer_query->row['source_name'] ;
				$this->account     = $customer_query->row['account'] ;
				$this->parent_id   = $customer_query->row['parent'] ;
				$this->rateInfo    = array( "main_rate"     => $customer_query->row['main_rate'],
											"deduct_rate"   => $customer_query->row['deduct_rate'],
											"kmfun_rate"    => $customer_query->row['kmfun_rate'],
											"transfer_rate" => $customer_query->row['transfer_rate'],
											"transfer_rate" => $customer_query->row['transfer_rate'],
											"agency_name"   => $customer_query->row['agency_name'],
										) ;
				$this->bankInfo    = array(
											"receive"      => $customer_query->row['receive'],
											"account_name" => $customer_query->row['account_name'],
											"bank_name"    => $customer_query->row['bank_name'],
											"bank_branch"  => $customer_query->row['bank_branch'],
											"bank_code"    => $customer_query->row['bank_code'],
											"bank_account" => $customer_query->row['bank_account'],
										) ;
			} else {
				$this->logout() ;
			}
		}
	}

	/**
	 * [login description]
	 * @param   [type]     $email    [description]
	 * @param   [type]     $password [description]
	 * @param   boolean    $override [description]
	 * @return  [type]               [description]
	 * @Another Angus
	 * @date    2019-02-13
	 */
	public function login($email, $password, $override = false) {
		// dump( "Dis fucntion login") ;
		if ($override) {
			// dump("override") ;
			$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND status = '1'");
		} else {
			// dump("no override") ;
			$SQLCmd = "SELECT * FROM tb_resource WHERE account='".$this->db->escape( $email)."' AND password='".$this->db->escape( $password)."' AND status = 1" ;
			$customer_query = $this->db->query( $SQLCmd) ;
		}

		if ($customer_query->num_rows) {
			$this->session->data['dis_id'] = $customer_query->row['idx'] ;

			$this->source_id   = $customer_query->row['idx'] ;
			$this->source_name = $customer_query->row['source_name'] ;
			$this->account     = $customer_query->row['account'] ;
			$this->parent_id   = $customer_query->row['parent'] ;
			$this->rateInfo    = array( "main_rate"     => $customer_query->row['main_rate'],
										"deduct_rate"   => $customer_query->row['deduct_rate'],
										"kmfun_rate"    => $customer_query->row['kmfun_rate'],
										"transfer_rate" => $customer_query->row['transfer_rate'],
										"transfer_rate" => $customer_query->row['transfer_rate'],
										"agency_name"   => $customer_query->row['agency_name'],
									) ;
			$this->bankInfo    = array(
										"receive"      => $customer_query->row['receive'],
										"account_name" => $customer_query->row['account_name'],
										"bank_name"    => $customer_query->row['bank_name'],
										"bank_branch"  => $customer_query->row['bank_branch'],
										"bank_code"    => $customer_query->row['bank_code'],
										"bank_account" => $customer_query->row['bank_account'],
									) ;


			$SQLCmd = "INSERT INTO sys_dis_login SET source_id=" . $this->db->escape( $this->source_id) . ", source_name='" . $this->db->escape( $this->source_name) . "', ip='" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', login_date=now()" ;
			$this->db->query( $SQLCmd) ;

			return true;
		} else {
			return false;
		}
	}

	/**
	 * [logout description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-02-14
	 */
	public function logout() {
		unset($this->session->data['dis_id']);

		$this->source_id   = "" ;
		$this->source_name = "" ;
		$this->account     = "" ;
		$this->parent_id   = "" ;
		$this->rateInfo    = "" ;
		$this->bankInfo    = "" ;
	}

	/**
	 * [isLogged description]
	 * @return  boolean    [description]
	 * @Another Angus
	 * @date    2019-02-13
	 */
	public function isLogged() {
		return $this->source_id ;
	}

	/**
	 * [getId description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-02-14
	 */
	public function getId() {
		return $this->source_id;
	}

	/**
	 * [getUserName description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-02-14
	 */
	public function getUserName() {
		return $this->source_name;
	}

	/**
	 * [getAccount description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-02-27
	 */
	public function getAccount() {
		return $this->account;
	}

	/**
	 * [hasPermission description]
	 * @param   [type]     $key   [description]
	 * @param   [type]     $value [description]
	 * @return  boolean           [description]
	 * @Another Angus
	 * @date    2019-02-22
	 */
	public function hasPermission($key, $value) {
		if (isset($this->permission[$key])) {
			return in_array($value, $this->permission[$key]);
		} else {
			// return false;
			return true;
		}
	}

}