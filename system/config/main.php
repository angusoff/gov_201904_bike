<?php
// Site
$_['site_base']         = HTTP_SERVER;
$_['site_ssl']          = HTTPS_SERVER;

// Database
$_['db_autostart']      = true;
$_['db_type']           = DB_DRIVER; // mpdo, mssql, mysql, mysqli or postgre
$_['db_hostname']       = DB_HOSTNAME;
$_['db_username']       = DB_USERNAME;
$_['db_password']       = DB_PASSWORD;
$_['db_database']       = DB_DATABASE;
$_['db_port']           = DB_PORT;

// Session
$_['session_autostart'] = true;

// Actions
$_['action_pre_action'] = array(
	'startup/startup',
	'startup/error',
	'startup/event',
	'startup/sass',
	// 'startup/login',
	'startup/permission',
	// 'report/personal'
);

// Actions
$_['action_default'] = 'common/dashboard';

// pagination
$_['config_limit_admin'] = 20;
$_['config_language_id'] = 1;

// 運動咖文章出處
$_['sports_source_default'] = array(
    'biji'        =>  '運動筆記',
    'i933'        =>  '933樂活網',
    'eventpal'    =>  '活動爆報',
    'news'        =>  '其他',
);

/**
 * Outputs the given variables with formatting and location. Huge props
 * out to Phil Sturgeon for this one (http://philsturgeon.co.uk/blog/2010/09/power-dump-php-applications).
 * To use, pass in any number of variables as arguments.
 *
 * @return void
 */
function dump() {
	list($callee) = debug_backtrace();
	$arguments = func_get_args();
	$total_arguments = count($arguments);

	echo '<fieldset style="background:#fefefe !important; border:2px red solid; padding:5px">' . PHP_EOL .
		'<legend style="background:lightgrey; padding:5px;">' . $callee['file'] . ' @ line: ' . $callee['line'] . '</legend>' . PHP_EOL .
		'<pre>';

    $i = 0;
    foreach ($arguments as $argument) {
		echo '<br/><strong>Debug #' . (++$i) . ' of ' . $total_arguments . '</strong>: ';

		if ( (is_array($argument) || is_object($argument)) && count($argument)) {
			print_r($argument);
		} else {
			var_dump($argument);
		}
	}

	echo '</pre>' . PHP_EOL .
		'</fieldset>' . PHP_EOL;
}