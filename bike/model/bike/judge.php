<?php
class ModelBikeJudge extends Model {
	/**
	 * $this->db->query ->
	 * 	rows		所有筆數
	 * 	row			單筆
	 * 	num_rows	筆數 cnt
	 */

	/**
	 * [checkJudgeAcc 評審委員 登入]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-04-20
	 */
	public function checkJudgeAcc( $data = array()) {
		$account  = $this->db->escape($data['account']) ;
		$password = $this->db->escape($data['password']) ;

		$SQLCmd = "SELECT * FROM tb_judge WHERE account='{$account}' and password='{$password}'" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd) ;
	}

	/**
	 * [getPlanLists 計劃列表]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-23
	 */
	public function getPlanLists( $data = array()) {
		// dump( $data) ;
		if ( $data['sort'] == 'idx') {
			$orderStr = "ORDER BY a.idx {$data['order']}" ;
		} else if ( $data['sort'] = 'grade') {
			$orderStr = "ORDER BY b.total {$data['order']}, a.idx ASC" ;
		}

		$SQLCmd = "SELECT a.idx, a.area, a.plan_name, a.doc_num, b.total FROM tb_plan_list a LEFT JOIN (
				SELECT PlanID, judge_user, total FROM tb_judge_answer WHERE idx IN (
				SELECT max(idx) FROM tb_judge_answer WHERE judge_user='{$data['judge_user']}' GROUP BY PlanID)) b
				on a.idx=b.PlanID {$orderStr}" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd)->rows ;
	}

	/**
	 * [getPlanName 取得計劃名稱]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-04-30
	 */
	public function getPlanName( $data = array()) {
		$SQLCmd = "SELECT * FROM tb_plan_list WHERE idx=". $this->db->escape($data['pid']) ;
		return $this->db->query( $SQLCmd)->row ;
	}

	/**
	 * [insertJudgerLog 登入紀錄]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-04-20
	 */
	public function insertJudgerLog( $data = array()) {
		$SQLCmd = "insert into tb_judge_login set account='". $this->db->escape( $data['account']) ."', login=now()" ;
		$this->db->query( $SQLCmd) ;
	}

	/**
	 * [getJudgeAnswer 委員評分結果]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-04-30
	 */
	public function getJudgeAnswer( $data = array()) {
		$SQLCmd = "SELECT * FROM tb_judge_answer WHERE planID='". $this->db->escape( $data['idx']) ."' and judge_user='". $this->db->escape( $data['judgeID']) ."' ORDER BY submit_time DESC LIMIT 1" ;
		// dump( $SQLCmd) ;
		$retObj = $this->db->query( $SQLCmd) ;
		return $retObj ;
	}

	/**
	 * [saveJudgeAns 委員評分存檔]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-04-30
	 */
	public function saveJudgeAns( $data = array()) {
		if ( !empty( $data['save_submit'])) {


		} else {
			$answerStr = json_encode( $data['answer']) ;
			$SQLCmd = "INSERT INTO tb_judge_answer SET planID='". $this->db->escape( $data['pid']) ."', judge_user='". $this->db->escape( $data['judge_user']) ."', answer='". $answerStr ."', total='{$data['total']}', submit_time=now()" ;
			// dump( $SQLCmd) ;
			$this->db->query( $SQLCmd) ;
		}
	}

	/**
	 * [getJudger 委員清單]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-30
	 */
	public function getJudger() {
		$SQLCmd = "select * from tb_judge" ;
		// dump( $SQLCmd) ;

		$retJudgeArr = $this->db->query( $SQLCmd)->rows ;
		$judger = array() ;
		foreach ($retJudgeArr as $iCnt => $tmp) {
			$judger[$tmp['account']] = $tmp['name'] ;
		}
		return $judger ;
	}

	/**
	 * [getJudgeScore description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-30
	 */
	public function getJudgeScore() {
		$SQLCmd = "SELECT PlanID, judge_user, total FROM tb_judge_answer WHERE idx IN (
				SELECT max(idx) FROM tb_judge_answer GROUP BY PlanID, judge_user)
				ORDER BY judge_user ASC, total DESC" ;
		// dump( $SQLCmd) ;

		$retJudgeArr = $this->db->query( $SQLCmd)->rows ;
		$judgeScore = array() ;
		foreach ($retJudgeArr as $iCnt => $tmp) {
			$judgeScore[$tmp['judge_user']][$tmp['PlanID']] = round( $tmp['total'], 2)  ;
		}
		return $judgeScore ;
	}
	/**
	 * [getJudgeScore description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-30
	 */
	public function getJudgeScore2() {
		$SQLCmd = "SELECT PlanID, judge_user, total, answer FROM tb_judge_answer WHERE idx IN (
				SELECT max(idx) FROM tb_judge_answer GROUP BY PlanID, judge_user)
				ORDER BY judge_user ASC, total DESC" ;
		// dump( $SQLCmd) ;

		$retJudgeArr = $this->db->query( $SQLCmd)->rows ;
		$judgeScore = array() ;
		foreach ($retJudgeArr as $iCnt => $tmp) {
			$judgeScore[$tmp['judge_user']][$tmp['PlanID']] = array(
				"total" => round( $tmp['total'], 2),
				"answer" => $tmp['answer'],
			);
		}
		return $judgeScore ;
	}
}