<?php
class ModelBikeJob extends Model {
	/**
	 * $this->db->query ->
	 * 	rows		所有筆數
	 * 	row			單筆
	 * 	num_rows	筆數 cnt
	 */

	public function getJudgeAnswer() {
		$SQLCmd = "SELECT a.PlanID, b.area, b.plan_name, a.judge_user, a.total, a.answer FROM tb_judge_answer a
				LEFT JOIN tb_plan_list b ON a.PlanID=b.idx
				WHERE a.idx IN (
					SELECT max(idx) FROM tb_judge_answer GROUP BY PlanID,judge_user
				) ORDER BY PlanID ASC" ;

		$retByJudgeArr = array() ;
		$retArr = $this->db->query( $SQLCmd)->rows ;
		foreach ($retArr as $iCnt => $answers) {
			$retByJudgeArr[$answers['PlanID']][$answers['judge_user']] = $answers ;
		}

		return $retByJudgeArr ;
	}

}