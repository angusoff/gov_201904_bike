<?php
class ModelBikeReport extends Model {
	/**
	 * $this->db->query ->
	 * 	rows		所有筆數
	 * 	row			單筆
	 * 	num_rows	筆數 cnt
	 */

	/**
	 * [getPlanLists description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-03-30
	 */
	public function getPlanLists() {
		$SQLCmd = "SELECT * FROM tb_plan_list ORDER BY idx ASC" ;
		// dump( $this->db->query( $SQLCmd)) ;
		return $this->db->query( $SQLCmd)->rows ;
	}

	/**
	 * [getPlanName description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-03-30
	 */
	public function getPlanName( $data = array()) {
		// dump( $data) ;
		$SQLCmd = "SELECT * FROM tb_plan_list WHERE idx=" . (int)$this->db->escape($data['idx']) ;
		return $this->db->query( $SQLCmd)->row ;
	}

	public function checkPlan( $data = array()) {
		$SQLCmd = "SELECT * FROM tb_plan_list WHERE idx=" . (int)$this->db->escape($data['idx']) . " AND doc_num='".$this->db->escape($data['doc_num'])."'" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd) ;
	}

	public function getAnswer( $data= array()) {
		$SQLCmd = "SELECT * FROM tb_plan_answer WHERE pid=" . (int)$this->db->escape($data['idx']) . " AND doc_num='".$this->db->escape($data['doc_num'])."' ORDER BY modify_date DESC LIMIT 1" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd) ;
	}

	public function getSelfAnswer( $data= array()) {
		$SQLCmd = "SELECT * FROM tb_plan_self_ans WHERE pid=" . (int)$this->db->escape($data['idx']) . " AND doc_num='".$this->db->escape($data['doc_num'])."' ORDER BY modify_date DESC LIMIT 1" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd) ;
	}

	public function getSelfAnswerOldVersion( $data= array()) {
		$SQLCmd = "SELECT * FROM tb_plan_self_ans WHERE pid=" . (int)$this->db->escape($data['idx']) . " AND doc_num='".$this->db->escape($data['doc_num'])."' AND modify_date < '2019-05-30' ORDER BY modify_date DESC LIMIT 1" ;
		// dump( $SQLCmd) ;
		return $this->db->query( $SQLCmd) ;
	}

	public function saveAnswer( $data = array()) {
		// dump($data) ;
		$setStr = "" ;

		$setStr .= !empty(trim( $data['pid'])) ? "pid='".$this->db->escape(trim($data['pid']))."'," : "" ;
		$setStr .= !empty(trim( $data['doc_num'])) ? "doc_num='".$this->db->escape(trim($data['doc_num']))."'," : "" ;

		foreach ($data['answer'] as $iCnt => $tmpRow) {
			$data['answer'][$iCnt] = !empty( trim( $tmpRow)) ? $this->db->escape(trim($tmpRow)) : "" ;
		}
		$jsonAnswer = urldecode( json_encode( $data['answer'])) ;
		$setStr .= "answer='" . $jsonAnswer . "'," ;

		$setStr .= !empty(trim( $data['startRoute'])) ? "startRoute='".$this->db->escape(trim($data['startRoute']))."'," : "" ;
		$setStr .= !empty(trim( $data['plan'])) ? "plan='".$this->db->escape(trim($data['plan']))."'," : "" ;
		$setStr .= !empty(trim( $data['planDate1'])) ? "planDate1='".$this->db->escape(trim($data['planDate1']))."'," : "" ;
		$setStr .= !empty(trim( $data['planDate2'])) ? "planDate2='".$this->db->escape(trim($data['planDate2']))."'," : "" ;
		$setStr .= !empty(trim( $data['planDate3'])) ? "planDate3='".$this->db->escape(trim($data['planDate3']))."'," : "" ;
		$setStr .= !empty(trim( $data['planDate4'])) ? "planDate4='".$this->db->escape(trim($data['planDate4']))."'," : "" ;
		$setStr .= !empty(trim( $data['planStatus'])) ? "planStatus='".$this->db->escape(trim($data['planStatus']))."'," : "" ;
		$setStr .= !empty(trim( $data['environment'])) ? "environment='".$this->db->escape(trim($data['environment']))."'," : "" ;
		$setStr .= !empty(trim( $data['route'])) ? "route='".$this->db->escape(trim($data['route']))."'," : "" ;

		$setStr .= "modify_date = now()" ;

		$SQLCmd = "INSERT INTO tb_plan_answer SET {$setStr}" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function saveSelfAnswer( $data = array()) {
		$setStr = "" ;

		$setStr .= !empty(trim( $data['pid'])) ? "pid='".$this->db->escape(trim($data['pid']))."'," : "" ;
		$setStr .= !empty(trim( $data['doc_num'])) ? "doc_num='".$this->db->escape(trim($data['doc_num']))."'," : "" ;

		foreach ($data['answer'] as $iCnt => $tmpRow) {
			$data['answer'][$iCnt] = !empty( trim( $tmpRow)) ? $this->db->escape(trim($tmpRow)) : "" ;
		}
		$jsonAnswer = json_encode( $data['answer']) ;
		$setStr .= "answer='" . $jsonAnswer . "'," ;
		$setStr .= "acc1_1='" . $this->db->escape(trim($data['acc1-1'])) . "'," ;
		$setStr .= "acc1_5='" . $this->db->escape(trim($data['acc1-5'])) . "'," ;
		$setStr .= "acc2_2='" . $this->db->escape(trim($data['acc2-2'])) . "'," ;
		$setStr .= "acc3_2='" . $this->db->escape(trim($data['acc3-2'])) . "'," ;
		$setStr .= "acc3_3='" . $this->db->escape(trim($data['acc3-3'])) . "'," ;
		$setStr .= "modify_date = now()" ;

		$SQLCmd = "INSERT INTO tb_plan_self_ans SET {$setStr}" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function updatePlan($data = array()) {
		$whereStr = "" ;
		$setStr   = "" ;

		$whereStr .= !empty(trim( $data['pid']))	? "idx='".$this->db->escape(trim($data['pid']))."'," : "" ;
		$whereStr .= !empty(trim( $data['doc_num']))	? "doc_num='".$this->db->escape(trim($data['doc_num']))."'," : "" ;

		$setStr .= !empty(trim( $data['organ']))	? "organ='".$this->db->escape(trim($data['organ']))."'," : "" ;
		$setStr .= !empty(trim( $data['name']))		? "name='".$this->db->escape(trim($data['name']))."'," : "" ;
		$setStr .= !empty(trim( $data['tel']))		? "tel='".$this->db->escape(trim($data['tel']))."'," : "" ;
		$setStr .= !empty(trim( $data['mobile']))	? "mobile='".$this->db->escape(trim($data['mobile']))."'," : "" ;
		$setStr .= !empty(trim( $data['mail']))		? "mail='".$this->db->escape(trim($data['mail']))."'," : "" ;

		$setStr = substr( $setStr, 0, strlen( $setStr)-1) ;

		$SQLCmd = "UPDATE  tb_plan_list SET {$setStr} WHERE idx='".$this->db->escape(trim($data['pid']))."' AND doc_num='".$this->db->escape(trim($data['doc_num']))."'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function updatePlanSelf($data =array()) {
		$whereStr = "" ;
		$setStr   = "" ;

		$whereStr .= !empty(trim( $data['pid']))	? "idx='".$this->db->escape(trim($data['pid']))."'," : "" ;
		$whereStr .= !empty(trim( $data['doc_num']))	? "doc_num='".$this->db->escape(trim($data['doc_num']))."'," : "" ;

		$setStr .= "self_time = now()" ;

		$SQLCmd = "UPDATE  tb_plan_list SET {$setStr} WHERE idx='".$this->db->escape(trim($data['pid']))."' AND doc_num='".$this->db->escape(trim($data['doc_num']))."'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function getPlanAnsStatus() {
		$retArr = Array() ;
		$SQLCmd = "SELECT idx, pid, modify_date mData FROM tb_plan_answer a WHERE idx IN (
					SELECT max(idx) FROM tb_plan_answer WHERE pid != '' GROUP BY pid)" ;
		$cntArr = $this->db->query( $SQLCmd)->rows ;
		// dump( $cntArr) ;
		foreach ($cntArr as $iCnt => $tmpRow) {
			if ( !empty( $tmpRow['pid'])) {
				$retArr[ $tmpRow['pid']]['evaluation']       = $tmpRow['idx'] ;
				$retArr[ $tmpRow['pid']]['evaluation_mData'] = $tmpRow['mData'] ;
			}
		}
		$SQLCmd = "SELECT idx, pid, modify_date mData FROM tb_plan_self_ans a WHERE idx IN (
					SELECT max(idx) FROM tb_plan_self_ans WHERE pid != '' GROUP BY pid)" ;
		$cntArr = $this->db->query( $SQLCmd)->rows ;
		foreach ($cntArr as $iCnt => $tmpRow) {
			if ( !empty( $tmpRow['pid'])) {
				$retArr[ $tmpRow['pid']]['checking']       = $tmpRow['idx'] ;
				$retArr[ $tmpRow['pid']]['checking_mData'] = $tmpRow['mData'] ;
			}
		}
		return $retArr ;
	}


}