<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> 委員評分結果</font>
				</div>
				<div class="card-body">

<div class="table-responsive">
	<table class="table table-bordered table-striped ">
		<thead class="thead-dark">
			<tr>
				<th>
					<?php if ( $sort == 'idx') : ?>
					<a href="<?=$sort_area?>" style="color:#fff; text-decoration:none;">#
						<i class="fa <?=$url_sort_icon?> fa-fw"></i>
					</a>
					<?php else : ?>
					<a href="<?=$sort_area?>" style="color:#fff; text-decoration:none;">#</a>
					<?php endif ; ?>
				</th>
				<th>地區</th>
				<th>計畫名稱</th>
				<?php foreach ($judgeScore as $judgeKey => $value) : ?>
					<!--<th><?=$judger[$judgeKey]?></th>-->
					<th><?=$judgeKey?></th>
				<?php endforeach ; ?>
				<th>序位得分</th>
				<th>
					<?php if ( $sort == 'rank') : ?>
					<a href="<?=$sort_rank?>" style="color:#fff; text-decoration:none;">名次
						<i class="fa <?=$url_sort_icon?> fa-fw"></i>
					</a>
					<?php else : ?>
					<a href="<?=$sort_rank?>" style="color:#fff; text-decoration:none;">名次</a>
					<?php endif ; ?>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($planList as $iCnt => $planRow) : ?>
			<tr>
				<td><?=$planRow['idx']?></td>
				<td><?=$planRow['area']?></td>
				<td><a href="?route=bike/judge/planInfoView&pid=<?=$planRow['idx']?>&doc_num=<?=$planRow['doc_num']?>" target="_blank"><?=$planRow['plan_name']?></a></td>
				<?php foreach ($judgeScore as $judgeKey => $scoreArr) : ?>
					<td>
						<?php if ( isset( $scoreArr[$planRow['idx']])) : ?>
							<?=$scoreArr[$planRow['idx']]?>
						<?php endif ; ?>
					</td>
				<?php endforeach ; ?>
				<td><?=isset( $ordinal[$planRow['idx']]) ? $ordinal[$planRow['idx']] : "" ;?></td>
				<td><a href="?route=bike/judge/spiderweb&pid=<?=$planRow['idx']?>" target="_blank">
					<?=isset( $rankArr[$planRow['idx']]) ? $rankArr[$planRow['idx']] : "" ;?></a>
				</td>
			</tr>
			<?php endforeach ; ?>

		</tbody>
	</table>
</div>

				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript">
</script>
<?php echo $footer; ?>