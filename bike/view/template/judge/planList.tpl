<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> 計畫列表</font>
				</div>
				<div class="card-body">
					<?php if ($error_warning) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
					<?php } ?>
			        <?php if ($success) { ?>
			        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			        <button type="button" class="close" data-dismiss="alert">&times;</button>
			        </div>
			        <?php } ?>

<div class="table-responsive">
	<table class="table table-bordered table-striped ">
		<thead class="thead-dark">
			<tr>
				<th><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></th>
				<th>#</th>
				<th>
					<?php if ( $sort == 'idx') : ?>
					<a href="<?=$sort_area?>" style="color:#fff; text-decoration:none;">地區
						<i class="fa <?=$url_sort_icon?> fa-fw"></i>
					</a>
					<?php else : ?>
					<a href="<?=$sort_area?>" style="color:#fff; text-decoration:none;">地區</a>
					<?php endif ; ?>
				</th>
				<th>計畫名稱</th>
				<th>
					<?php if ( $sort == 'grade') : ?>
					<a href="<?=$sort_grade?>" style="color:#fff; text-decoration:none;">評分
						<i class="fa <?=$url_sort_icon?> fa-fw"></i>
					</a>
					<?php else : ?>
					<a href="<?=$sort_grade?>" style="color:#fff; text-decoration:none;">評分</a>
					<?php endif ; ?>
				</th>
				<th>自評表 / 自檢表</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($planList as $iCnt => $planRow) : ?>
			<tr>
				<td><input type="checkbox" name="selected[]" value="" /></td>
				<td><?=$planRow['idx']?></td>
				<td><?=$planRow['area']?></td>
				<td><?=$planRow['plan_name']?></td>
				<td><?=$planRow['total']?></td>
				<td><?=$planRow['viewButton']?></td>
			</tr>
			<?php endforeach ; ?>
		</tbody>
	</table>
</div>

				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript">
</script>
<?php echo $footer; ?>