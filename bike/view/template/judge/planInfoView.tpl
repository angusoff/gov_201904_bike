<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> <font color="red"><?=$planRow['plan_name']?></font>
				</div>
				<div class="card-body">
					<form id="form" method="post" action="" enctype="multipart/form-data">
<div class="table-responsive">
	<a class="btn btn-primary mb-2" href="doc/<?=$pid?>.pdf" target="_blank">計畫書下載</a>
	<table class="table table-bordered table-striped table-sm" style="table-layout: fixed;">
		<thead class="thead-dark">
			<tr>
				<th width="50%">自評表</th>
				<th width="50%">自檢表</th>
			</tr>
		</thead>
		<tbody>
			<tr>
<!-- 自評表 -->
				<td>
<!-- 自評表 body -->
<div class="card">
	<div class="card-body">
		<h5 class="card-title">計畫總長度</h5>
		<p class="card-text text-primary"><?=$item1?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">計畫起訖路線(含路徑)</h5>
		<p class="card-text text-primary"><?=$startRoute?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">計畫項目</h5>
		<p class="card-text text-primary"><?=$plan?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">計畫核定日期</h5>
		<p class="card-text text-primary"><?=$planDate1?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">完成規劃設計日期</h5>
		<p class="card-text text-primary"><?=$planDate2?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">完成招標日期</h5>
		<p class="card-text text-primary"><?=$planDate3?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">開工日期</h5>
		<p class="card-text text-primary"><?=$planDate4?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">計畫路線特色_環境特色</h5>
		<p class="card-text text-primary"><?=$environment?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">計畫路線特色_騎乘與串聯路線</h5>
		<p class="card-text text-primary"><?=$route?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">自行車道新（整）建項目</h5>
		<p class="card-text text-primary"><?=$item2?></p>
	</div>
</div>
<hr>
<label>問卷項目<br/>(請簡述標地物名稱及設置位置)</label>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">景觀建物</h5>
		<h5 class="card-title">車道沿線是否具景觀建物 : 如景觀大橋、隧道、鐵道、特色建物…等</h5>
		<p class="card-text text-primary"><?=$item4?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">打卡地標</h5>
		<h5 class="card-title">車道沿線是否具拍照打卡地標或建物 : 如立體文字地標、趣味、指標性建物…等</h5>
		<p class="card-text text-primary"><?=$item5?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">特色風景</h5>
		<h5 class="card-title">車道沿線是否具特色風景 : 如夕陽、湖泊、海灘、田園、季節性風景(花草樹木)…等</h5>
		<p class="card-text text-primary"><?=$item6?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">親子遊憩</h5>
		<h5 class="card-title">車道沿線是否具親子玩樂點 : 如公園、沙灘、溜滑梯…等</h5>
		<p class="card-text text-primary"><?=$item7?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">自然生態</h5>
		<h5 class="card-title">車道沿線是否具自然生態保育區 : 如林地、濕地、棲地保育區…等</h5>
		<p class="card-text text-primary"><?=$item8?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">車道串連</h5>
		<h5 class="card-title">車道是否具與其他縣市自行車道可串接性 (請寫出可串接路線名稱)</h5>
		<p class="card-text text-primary"><?=$item9?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">獨立車道</h5>
		<h5 class="card-title">車道是否70％以上為專用自行車道? 若否，請寫出非獨立自行車道路段及公里數</h5>
		<p class="card-text text-primary"><?=$item10?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">交通運輸</h5>
		<h5 class="card-title">起終點是否具停車場、大眾交通運輸工具接駁 (請簡述大眾運輸工具及站名、停車場名稱)</h5>
		<p class="card-text text-primary"><?=$item11?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">在地特色</h5>
		<h5 class="card-title">車道沿線是否具在地文化或特色景點(廟宇、特色眷村、農漁村聚落…等)</h5>
		<p class="card-text text-primary"><?=$item12?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">周遭結合</h5>
		<h5 class="card-title">鄰近自行車道1公里內具文化、遊憩景點、固定性舉辦活動及地知名商家(遊樂區、觀光工廠…等)</h5>
		<p class="card-text text-primary"><?=$item13?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">美食補給</h5>
		<h5 class="card-title">車道周邊是否具美食補給點(地方小吃…等)</h5>
		<p class="card-text text-primary"><?=$item14?></p>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<h5 class="card-title">其他</h5>
		<h5 class="card-title">車道沿線是否有雙語標示、廁所(固定、流動式)、醫療單位、住宿等，請說明</h5>
		<p class="card-text text-primary"><?=$item15?></p>
	</div>
</div>

				</td>
<!-- 自檢表 -->
				<td style="vertical-align: top;">
<!-- 自檢表 body -->
<div class="card selfCheckCard">
	<div class="card-body">
		<div class="row mb-2 checkCardTitle">
			<div class="descField"></div>
			<div class="percentField">權重</div>
			<div class="checkField">檢核</div>
		</div>
		<h5 class="card-title row">
			<div class="descField">騎乘路線規劃</div>
			<div class="percentField">25%</div>
			<div class="checkField"></div>
		</h5>
		<p class="card-text">
			<div class="row mb-2">
				<div class="descField">
					1.起訖指標是否清楚明確、無障礙標示是否明確
				</div>
				<div class="percentField">20%</div>
				<div class="checkField"><?=$opt1?></div>
			</div>
			<div class="row mb-2">
				<div class="descField text-success"">
					<span class="text-primary">無障礙補充說明：</span><?=$acc1_1?>
				</div>
			</div>
			<div class="row mb-2">
				<div class="descField">
					2.起(終)點是否有公共交通運輸
				</div>
				<div class="percentField">10%</div>
				<div class="checkField"><?=$opt2?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">
					3.沿線是否有2座以上導覽說明牌
				</div>
				<div class="percentField">20%</div>
				<div class="checkField"><?=$opt3?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">
					4.是否有休憩點設置
				</div>
				<div class="percentField">10%</div>
				<div class="checkField"><?=$opt4?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">
					5.休憩點是否有公(流)廁、自行車停車架、無障斜坡道、無障礙座椅席位、無障礙停車位
				</div>
				<div class="percentField">15%</div>
				<div class="checkField"><?=$opt5?></div>
			</div>
			<div class="row mb-2">
				<div class="descField text-success"">
					<span class="text-primary">無障礙補充說明：</span><?=$acc1_5?>
				</div>
			</div>
			<div class="row mb-2">
				<div class="descField">
					6.全線告示/指示牌(雙語)是否明確:公里、方向、指示牌等等
				</div>
				<div class="percentField">25%</div>
				<div class="checkField"><?=$opt6?></div>
			</div>
			<div class="row mb-2">
				<div class="col-12" id="opt1">
				</div>
			</div>
		</p>
	</div>
</div>
<div class="card selfCheckCard">
	<div class="card-body">
		<h5 class="card-title row">
			<div class="descField">周邊服務</div>
			<div class="percentField">10%</div>
			<div class="checkField"></div>
		</h5>
		<p class="card-text">
			<div class="row mb-2">
				<div class="descField">1.是否有自行車租賃站(點)</div>
				<div class="percentField">20%</div>
				<div class="checkField"><?=$opt7?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">2.周邊是否有停車場、無障礙停車位</div>
				<div class="percentField">20%</div>
				<div class="checkField"><?=$opt8?></div>
			</div>
			<div class="row mb-2">
				<div class="descField text-success"">
					<span class="text-primary">無障礙補充說明：</span><?=$acc2_2?>
				</div>
			</div>
			<div class="row mb-2">
				<div class="descField">3.沿線是否有公(流)廁及無障礙廁所</div>
				<div class="percentField">40%</div>
				<div class="checkField"><?=$opt9?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">4.是否有遊客中心</div>
				<div class="percentField">20%</div>
				<div class="checkField"><?=$opt10?></div>
			</div>
			<div class="row mb-2">
				<div class="col-12" id="opt2">
				</div>
			</div>
		</p>
	</div>
</div>
<div class="card selfCheckCard">
	<div class="card-body">
		<h5 class="card-title row">
			<div class="descField">路面與安全設施</div>
			<div class="percentField">25%</div>
			<div class="checkField"></div>
		</h5>
		<p class="card-text">
			<div class="row mb-2">
				<div class="descField">1.沿線有便利商店或補給站(含鐵馬驛站)</div>
				<div class="percentField">15%</div>
				<div class="checkField"><?=$opt11?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">2.車道路面是否平整、無障礙通路</div>
				<div class="percentField">25%</div>
				<div class="checkField"><?=$opt12?></div>
			</div>
			<div class="row mb-2">
				<div class="descField text-success"">
					<span class="text-primary">無障礙補充說明：</span><?=$acc3_2?>
				</div>
			</div>
			<div class="row mb-2">
				<div class="descField">3.全線自行車(專用)道標誌/標線是否足夠、無障礙標示</div>
				<div class="percentField">25%</div>
				<div class="checkField"><?=$opt13?></div>
			</div>
			<div class="row mb-2">
				<div class="descField text-success"">
					<span class="text-primary">無障礙補充說明：</span><?=$acc3_3?>
				</div>
			</div>
			<div class="row mb-2">
				<div class="descField">4.是否有巡邏站</div>
				<div class="percentField">15%</div>
				<div class="checkField"><?=$opt14?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">5.設施維護：護欄、照明、邊坡安全…等</div>
				<div class="percentField">20%</div>
				<div class="checkField"><?=$opt15?></div>
			</div>
			<div class="row mb-2">
				<div class="col-12" id="opt3">
				</div>
			</div>
		</p>
	</div>
</div>
<div class="card selfCheckCard">
	<div class="card-body">
		<h5 class="card-title row">
			<div class="descField">車道串連</div>
			<div class="percentField">20%</div>
			<div class="checkField"></div>
		</h5>
		<p class="card-text">
			<div class="row mb-2">
				<div class="descField">1.可串接區域環線</div>
				<div class="percentField">50%</div>
				<div class="checkField"><?=$opt16?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">2.可串接鄰近縣市自行車道</div>
				<div class="percentField">25%</div>
				<div class="checkField"><?=$opt17?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">3.可串接環島線</div>
				<div class="percentField">25%</div>
				<div class="checkField"><?=$opt18?></div>
			</div>
			<div class="row mb-2">
				<div class="col-12" id="opt4">
				</div>
			</div>
		</p>
	</div>
</div>
<div class="card selfCheckCard">
	<div class="card-body">
		<h5 class="card-title row">
			<div class="descField">景點與在地特色</div>
			<div class="percentField">20%</div>
			<div class="checkField"></div>
		</h5>
		<p class="card-text">
			<div class="row mb-2">
				<div class="descField">1.人文歷史文化景點:特殊建物、打卡標地、在地文化、古蹟文物、特色景點</div>
				<div class="percentField">25%</div>
				<div class="checkField"><?=$opt19?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">2.地理景觀:特殊地形、奇特地貌</div>
				<div class="percentField">25%</div>
				<div class="checkField"><?=$opt20?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">3.自然生態: 生態公園、濕地、森林步道、水庫、動(植)物、水鳥保護區等</div>
				<div class="percentField">25%</div>
				<div class="checkField"><?=$opt21?></div>
			</div>
			<div class="row mb-2">
				<div class="descField">4.沿線或鄰近有在地美食及商家</div>
				<div class="percentField">25%</div>
				<div class="checkField"><?=$opt22?></div>
			</div>
			<div class="row mb-2">
				<div class="col-12" id="opt5">
				</div>
			</div>
		</p>
	</div>
</div>
<input type="hidden" id="save_submit" name="save_submit" >
<input type="hidden" name="pid" value="<?=$pid?>">

				</td>
			</tr>
		</tbody>
	</table>
</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript">
$('.kDown').keyup( function () {
	var check = true ;
	var re = /^[0-9]+$/;
	if ( !re.test( $(this).val()) && $(this).val() != "") {
		alert("只能輸入數字");
		$(this).val( "") ;
		check = false ;
  	}
  	if ( parseInt( $(this).val()) > 100) {
  		alert("輸入不能超過100");
  		check = false ;
  	}
  	if ( check) {
		calculation() ;
  	}
});


function calculation () {
	var opt1 = 0 ;
	var opt2 = 0 ;
	var opt3 = 0 ;
	var opt4 = 0 ;
	var opt5 = 0 ;

	opt1 += $('#opt1-1').val() * 0.2 ;
	opt1 += $('#opt1-2').val() * 0.1 ;
	opt1 += $('#opt1-3').val() * 0.2 ;
	opt1 += $('#opt1-4').val() * 0.1 ;
	opt1 += $('#opt1-5').val() * 0.15 ;
	opt1 += $('#opt1-6').val() * 0.25 ;
	var ratio1 = ( opt1 * 0.25) ;
	if (opt1 > 0) {
		// $('#opt1').html("合計 : <font color=red>" + opt1.toFixed(2) + "</font> 佔比 : <font color=red>" + ratio1.toFixed(2) + "</font>") ;
		$('#opt1').html("權重後小計 : <font color=red>" + opt1.toFixed(2) + "</font> ") ;
	}

	opt2 += $('#opt2-1').val() * 0.2 ;
	opt2 += $('#opt2-2').val() * 0.2 ;
	opt2 += $('#opt2-3').val() * 0.4 ;
	opt2 += $('#opt2-4').val() * 0.2 ;
	var ratio2 = ( opt2 * 0.1) ;
	if (opt2 > 0) {
		$('#opt2').html("權重後小計 : <font color=red>" + opt2.toFixed(2) + "</font> ") ;
	}

	opt3 += $('#opt3-1').val() * 0.15 ;
	opt3 += $('#opt3-2').val() * 0.25 ;
	opt3 += $('#opt3-3').val() * 0.25 ;
	opt3 += $('#opt3-4').val() * 0.15 ;
	opt3 += $('#opt3-5').val() * 0.2 ;
	var ratio3 = ( opt3 * 0.25) ;
	if (opt3 > 0) {
		$('#opt3').html("權重後小計 : <font color=red>" + opt3.toFixed(2) + "</font> ") ;
	}

	opt4 += $('#opt4-1').val() * 0.5 ;
	opt4 += $('#opt4-2').val() * 0.25 ;
	opt4 += $('#opt4-3').val() * 0.25 ;
	var ratio4 = ( opt4 * 0.2) ;
	if (opt4 > 0) {
		$('#opt4').html("權重後小計 : <font color=red>" + opt4.toFixed(2) + "</font> ") ;
	}

	opt5 += $('#opt5-1').val() * 0.25 ;
	opt5 += $('#opt5-2').val() * 0.25 ;
	opt5 += $('#opt5-3').val() * 0.25 ;
	opt5 += $('#opt5-4').val() * 0.25 ;
	var ratio5 = ( opt5 * 0.2) ;
	if (opt5 > 0) {
		$('#opt5').html("權重後小計 : <font color=red>" + opt5.toFixed(2) + "</font> ") ;
	}
	var total_opt = parseFloat(ratio1) + parseFloat(ratio2) + parseFloat(ratio3) + parseFloat(ratio4) + parseFloat(ratio5) ;
	console.log( total_opt);
	$('#total_opt').html("權重後總分 : <font color=red>" + total_opt.toFixed(2) + "</font>") ;
}

$( document ).ready(function() {
    calculation() ;
});
</script>
<?php echo $footer; ?>