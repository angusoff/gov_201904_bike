<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> 自行車道自我評量表 - <font color="red"><?=$planRow['plan_name']?></font>
				</div>
				<div class="card-body">
					<?php if ($error_warning) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
					<?php } ?>
			        <?php if ($success) { ?>
			        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			        <button type="button" class="close" data-dismiss="alert">&times;</button>
			        </div>
			        <?php } ?>

					<form id="form1" method="post" action="" enctype="multipart/form-data">
<div class="form-group">
	<div class="form-label-group">
		<input type="text" id="item1" name="item1" value="<?=$item1?>" class="form-control" placeholder="計畫總長度" autofocus="autofocus">
		<label for="item1">計畫總長度</label>
	</div>
</div>
<!-- Angus -->
<div class="form-group">
	<div class="form-label-group">
		<textarea id="startRoute" name="startRoute" class="form-control md-textarea" placeholder="計畫起訖路線(含路徑)" rows="3"><?=$startRoute?></textarea>
		<label for="startRoute">計畫起訖路線(含路徑)</label>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="plan" name="plan" class="form-control md-textarea" placeholder="計畫項目" rows="3"><?=$plan?></textarea>
		<label for="plan">計畫項目</label>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<input type="date" id="planDate1" name="planDate1" class="form-control noDate" placeholder="計畫核定日期" value="<?=$planDate1?>">
		<label for="planDate1">計畫核定日期</label>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<input type="date" id="planDate2" name="planDate2" class="form-control noDate" placeholder="完成規劃設計日期" value="<?=$planDate2?>">
		<label for="planDate2">完成規劃設計日期</label>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<input type="date" id="planDate3" name="planDate3" class="form-control noDate" placeholder="完成招標日期" value="<?=$planDate3?>">
		<label for="planDate3">完成招標日期</label>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<input type="date" id="planDate4" name="planDate4" class="form-control noDate" placeholder="開工日期" value="<?=$planDate4?>">
		<label for="planDate4">開工日期</label>
	</div>
</div>
<div class="form-group">
	<!-- <label for="sel1">Select list:</label> -->
	<select class="form-control custom-select" id="planStatus" name="planStatus">
		<option value="-1">目前計畫執行進度</option>
		<option value="0" <?= ($planStatus == '0') ? "selected" : "" ;?>>規劃中</option>
		<option value="1" <?= ($planStatus == '1') ? "selected" : "" ;?>>招標中</option>
		<option value="2" <?= ($planStatus == '2') ? "selected" : "" ;?>>施工中</option>
		<option value="3" <?= ($planStatus == '3') ? "selected" : "" ;?>>驗收與改善中</option>
		<option value="4" <?= ($planStatus == '4') ? "selected" : "" ;?>>已經啟用</option>
	</select>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="environment" name="environment" class="form-control md-textarea" placeholder="計畫路線特色_環境特色" rows="3" ><?=$environment?></textarea>
		<label for="environment">計畫路線特色_環境特色</label>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="route" name="route" class="form-control md-textarea" placeholder="計畫路線特色_騎乘與串聯路線" rows="3" ><?=$route?></textarea>
		<label for="route">計畫路線特色_騎乘與串聯路線</label>
	</div>
</div>
<!-- Angus -->


<div class="form-group">
	<div class="form-label-group">
		<textarea id="item2" name="item2" class="form-control md-textarea" placeholder="自行車道新（整）建項目" rows="3"><?=$item2?></textarea>
		<label for="item2">自行車道新（整）建項目</label>
	</div>
</div>
<hr>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item3" class="form-control md-textarea" placeholder="問卷項目" rows="3" readonly=""></textarea>
        <label for="item3">問卷項目<br/>(請簡述標地物名稱及設置位置)</label>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<!-- <input type="text" id="item4" class="form-control" placeholder="景觀建物（車道沿線是否具景觀建物 : 如景觀大橋、隧道、鐵道、特色建物…等）" required="required"> -->
		<textarea id="item4" name="item4" class="form-control md-textarea" placeholder="景觀建物" rows="3" ><?=$item4?></textarea>
		<label for="item4">景觀建物</label>
		<small id="emailHelp" class="form-text text-muted">車道沿線是否具景觀建物 : 如景觀大橋、隧道、鐵道、特色建物…等</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item5" name="item5" class="form-control md-textarea" placeholder="打卡地標" rows="3"><?=$item5?></textarea>
		<label for="item5">打卡地標</label>
		<small id="emailHelp" class="form-text text-muted">車道沿線是否具拍照打卡地標或建物 : 如立體文字地標、趣味、指標性建物…等</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item6" name="item6" class="form-control md-textarea" placeholder="特色風景" rows="3"><?=$item6?></textarea>
		<label for="item6">特色風景</label>
		<small id="emailHelp" class="form-text text-muted">車道沿線是否具特色風景 : 如夕陽、湖泊、海灘、田園、季節性風景(花草樹木)…等</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item7" name="item7" class="form-control md-textarea" placeholder="親子遊憩" rows="3"><?=$item7?></textarea>
		<label for="item7">親子遊憩</label>
		<small id="emailHelp" class="form-text text-muted">車道沿線是否具親子玩樂點 : 如公園、沙灘、溜滑梯…等</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item8" name="item8" class="form-control md-textarea" placeholder="自然生態" rows="3"><?=$item8?></textarea>
		<label for="item8">自然生態</label>
		<small id="emailHelp" class="form-text text-muted">車道沿線是否具自然生態保育區 : 如林地、濕地、棲地保育區…等</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item9" name="item9" class="form-control md-textarea" placeholder="車道串連" rows="3"><?=$item9?></textarea>
		<label for="item9">車道串連</label>
		<small id="emailHelp" class="form-text text-muted">車道是否具與其他縣市自行車道可串接性 (請寫出可串接路線名稱)</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item10" name="item10" class="form-control md-textarea" placeholder="獨立車道" rows="3"><?=$item10?></textarea>
		<label for="item10">獨立車道</label>
		<small id="emailHelp" class="form-text text-muted">車道是否70％以上為專用自行車道? 若否，請寫出非獨立自行車道路段及公里數</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item11" name="item11" class="form-control md-textarea" placeholder="交通運輸" rows="3"><?=$item11?></textarea>
		<label for="item11">交通運輸</label>
		<small id="emailHelp" class="form-text text-muted">起終點是否具停車場、大眾交通運輸工具接駁 (請簡述大眾運輸工具及站名、停車場名稱)</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item12" name="item12" class="form-control md-textarea" placeholder="在地特色" rows="3"><?=$item12?></textarea>
		<label for="item12">在地特色</label>
		<small id="emailHelp" class="form-text text-muted">車道沿線是否具在地文化或特色景點(廟宇、特色眷村、農漁村聚落…等)</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item13" name="item13" class="form-control md-textarea" placeholder="周遭結合" rows="3"><?=$item13?></textarea>
		<label for="item13">周遭結合</label>
		<small id="emailHelp" class="form-text text-muted">鄰近自行車道1公里內具文化、遊憩景點、固定性舉辦活動及地知名商家(遊樂區、觀光工廠…等)</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item14" name="item14" class="form-control md-textarea" placeholder="美食補給" rows="3"><?=$item14?></textarea>
		<label for="item14">美食補給</label>
		<small id="emailHelp" class="form-text text-muted">車道周邊是否具美食補給點(地方小吃…等)</small>
	</div>
</div>
<div class="form-group">
	<div class="form-label-group">
		<textarea id="item15" name="item15" class="form-control md-textarea" placeholder="其他" rows="3"><?=$item15?></textarea>
		<label for="item15">其他</label>
		<small id="emailHelp" class="form-text text-muted">車道沿線是否有雙語標示、廁所(固定、流動式)、醫療單位、住宿等，請說明</small>
	</div>
</div>

<div class="card-footer showArea">
	<p>註:</p>
	<ol>
		<li><font color="red">表單填寫後，請於最後一關設定填寫連絡窗口相關資訊</font></li>
	</ol>
</div>
<div class="form-group showArea">
	<div class="form-label-group">
		<input type="text" id="contact_organ" name="contact_organ" value="" class="form-control" placeholder="機關名稱" >
		<label for="contact_organ">機關名稱</label>
	</div>
</div>
<div class="form-group showArea">
	<div class="form-label-group">
		<input type="text" id="contact_name" name="contact_name" value="" class="form-control" placeholder="承辦人員" >
		<label for="contact_name">承辦人員</label>
	</div>
</div>
<div class="form-group showArea">
	<div class="form-label-group">
		<input type="text" id="contact_tel" name="contact_tel" value="" class="form-control" placeholder="市內電話" >
		<label for="contact_tel">聯絡電話 - 市內電話</label>
	</div>
</div>
<div class="form-group showArea">
	<div class="form-label-group">
		<input type="text" id="contact_mobile" name="contact_mobile" value="" class="form-control" placeholder="行動電話" >
		<label for="contact_mobile">聯絡電話 - 行動電話</label>
	</div>
</div>
<div class="form-group showArea">
	<div class="form-label-group">
		<input type="text" id="contact_mail" name="contact_mail" value="" class="form-control" placeholder="Email">
		<label for="contact_mail">Email</label>
	</div>
</div>

<input type="hidden" id="save_submit" name="save_submit" >
<?php if ($showButton) : ?>
<p>您可編輯部分內容後，按下【儲存】系統會將已編輯的內容暫存而不會送出。下次登入仍可繼續編輯或修改內容。<br>
按下【送出】後，即將內容上傳到系統，無法再做編輯修改，請確認所有內容都已編輯完畢再按送出。</p>
<button type="submit" class="btn btn-primary btn-block" tip="">儲存</button>
<button type="button" class="btn btn-success btn-block" onclick="submitFunction()">送出</button>
<?php endif ; ?>
					</form>
				</div>
<div class="card-footer">
	<p>註:</p>
	<ol>
		<li>問卷表答覆請盡可能詳細敘述，使專家學者能加速了解車道狀況。</li>
		<li>請於收文後依指定網址及日期線上填寫完成，以利後續輔導計畫進度。</li>
		<li>填寫若有任何問題，請洽聯網國際資訊股份有限公司 何小姐 02-87871315 #301</li>
	</ol>
</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->

<style>
</style>

<script type="text/javascript">
$('.showArea').hide() ;

function submitFunction() {

	if ($('#save_submit').val() == "" ) {
		$('.showArea').show()
		$('#save_submit').val("submit") ;
		alert( "請填寫 填寫連絡窗口相關資訊") ;
	} else {
		var msg = "" ;
		$("input[name*='contact']").each( function (idx) {
			if ( $(this).val() == "" ) {
				msg = "請填寫 填寫連絡窗口相關資訊" ;
			}
		}) ;
		if ( msg == "") {
			if ( confirm("資料送出後即無法修改")) {
				$('#form1').submit() ;
			}
		} else {
			alert(msg) ;
		}
	}
}
</script>
<?php echo $footer; ?>