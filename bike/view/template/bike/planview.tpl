<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> 填報進度表</font>
				</div>
				<div class="card-body">
					<?php if ($error_warning) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
					<?php } ?>

<div class="table-responsive">
	<table class="table table-bordered table-striped table-sm">
		<thead class="thead-dark">
			<tr>
				<th>#</th>
				<th>地區</th>
				<th>計畫名稱</th>
				<th>自評表</th>
				<th>自檢表</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($planList as $iCnt => $planRow) : ?>
			<tr>
				<td><?=$iCnt+1?></td>
				<td><?=$planRow['area']?>-<?=$planRow['idx']?></td>
				<td><?=$planRow['plan_name']?></td>
				<td><?=$planRow['evaluation_mData']?></td>
				<td><?=$planRow['checking_mData']?></td>
			</tr>
			<?php endforeach ; ?>
		</tbody>
	</table>
</div>

				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript">
</script>
<?php echo $footer; ?>