<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> 自行車道自我檢核表 - <font color="red"><?=$planRow['plan_name']?></font>
				</div>
				<div class="card-body">
					<?php if ($error_warning) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
					<?php } ?>
			        <?php if ($success) { ?>
			        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			        <button type="button" class="close" data-dismiss="alert">&times;</button>
			        </div>
			        <?php } ?>

					<form id="form1" method="post" action="" enctype="multipart/form-data">
<div class="checkList">
	<?php foreach ($qList as $iCnt => $qStr) : ?>
	<div class="listItem row">
		<div class="col-md-8 col-xs-12">
			<label class="questions"><?=$qStr?></label>
		</div>
		<div class="col-md-4 col-xs-12 text-right">
			<div class="btn-group btn-group-toggle" data-toggle="buttons">
				<?php
					$name = "opt".($iCnt+1) ;
					// dump( $name) ;
					$radioChecked = "" ;
					$activeStr    = "" ;
					if ( $$name == "") {
						$radioChecked = "checked" ;
						$activeStr    = "active" ;
					} else {
						$radioChecked = "" ;
						$activeStr    = "" ;
					}
				?>
				<label class="btn btn-light <?=$activeStr?>">
					<input type="radio" name="opt<?=$iCnt+1?>" value="" autocomplete="off" <?=$radioChecked?>>未作答
				</label>
				<?php
					if ( $$name == "O") {
						$radioChecked = "checked" ;
						$activeStr    = "active" ;
					} else {
						$radioChecked = "" ;
						$activeStr    = "" ;
					}
				?>
				<label class="btn btn-light <?=$activeStr?>">
					<input type="radio" name="opt<?=$iCnt+1?>" value="O" autocomplete="off" <?=$radioChecked?>>O
				</label>
				<?php
					if ( $$name == "X") {
						$radioChecked = "checked" ;
						$activeStr    = "active" ;
					} else {
						$radioChecked = "" ;
						$activeStr    = "" ;
					}
				?>
				<label class="btn btn-light <?=$activeStr?>">
					<input type="radio" name="opt<?=$iCnt+1?>" value="X" autocomplete="off" <?=$radioChecked?>>X
				</label>
			</div>
		</div>
	</div>

	<?php endforeach ; ?>
	<input type="hidden" id="save_submit" name="save_submit" >
	<?php if ($showButton) : ?>
<p>您可編輯部分內容後，按下【儲存】系統會將已編輯的內容暫存而不會送出。下次登入仍可繼續編輯或修改內容。<br>
按下【送出】後，即將內容上傳到系統，無法再做編輯修改，請確認所有內容都已編輯完畢再按送出。</p>
	<button type="submit" class="btn btn-primary btn-block" >儲存</button>
	<button type="button" class="btn btn-success btn-block" onclick="submitFunction()">送出</button>
	<?php endif ; ?>
					</form>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript">
function submitFunction() {
	$('#save_submit').val("submit") ;
	$('#form1').submit() ;
}


// $('#btSubmit').click( function () {
// 	console.log('test');
// 	$("input:radio[name*='opt']").each( function (idx) {
// 		var name = $(this).attr("name");
// 		console.log( name) ;




// 	}) ;
// 	$('#form1').submit() ;
	// $("input[name*='contact']").each( function (idx) {
</script>
<?php echo $footer; ?>