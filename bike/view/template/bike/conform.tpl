<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> <font color="red"><?=$planRow['plan_name']?></font>
				</div>
				<div class="card-body">
					<?php if ($error_warning) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
					<?php } ?>

					<form id="form" method="post" action="" enctype="multipart/form-data">
						<div class="form-group">
							<div class="form-label-group">
								<input type="text" id="item1" name="item1" class="form-control" placeholder="請輸入核定文號" required="required" autofocus="autofocus">
								<label for="item1">請輸入核定文號</label>
							</div>
						</div>
						<div class="form-group">
							<div class="form-label-group">
								<button type="submit" class="btn btn-primary btn-block">登入</button>
							</div>
						</div>
						<div class="form-group">
							<div class="form-label-group">
	<p>註:</p>
	<ol>
		<li>問卷表答覆請盡可能詳細敘述，使專家學者能加速了解車道狀況。</li>
		<li>請於收文後依指定網址及日期線上填寫完成，以利後續輔導計畫進度。</li>
		<li>填寫若有任何問題，請洽聯網國際資訊股份有限公司 何小姐 02-87871315 #301</li>
	</ol>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript">
</script>
<?php echo $footer; ?>