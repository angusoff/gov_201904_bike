<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> 自行車道自我檢核表 - <font color="red"><?=$planRow['plan_name']?></font>
				</div>
				<div class="card-body">
					<form id="form1" method="post" action="" enctype="multipart/form-data">

<div class="checkList">
	<div class="category">
		<div class="categoryTitle">騎乘路線規劃</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.起訖點標示是否明確</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt1 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt1 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt1" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt1" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt1" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.起(終)點是否有公共交通運輸</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt2 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt2 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt2" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt2" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt2" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.沿線是否有2座以上導覽說明牌</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt3 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt3 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt3" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt3" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt3" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">4.是否有休憩點設置</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt4 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt4 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt4" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt4" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt4" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">5.休憩點是否有公(流)廁、自行車停車架</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt5 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt5 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt5" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt5" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt5" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">6.全線告示/指示牌(雙語)是否明確:公里、方向、指示牌等等</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt6 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt6 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt6" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt6" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt6" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="category">
		<div class="categoryTitle">周邊服務</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.是否有自行車租賃站(點)</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt7 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt7 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt7" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt7" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt7" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.周邊是否有停車場</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt8 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt8 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt8" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt8" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt8" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.沿線是否有公(流)廁及無障礙廁所</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt9 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt9 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt9" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt9" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt9" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">4.是否有遊客中心</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt10 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt10 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt10" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt10" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt10" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="category">
		<div class="categoryTitle">路面與安全設施</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.沿線有便利商店或補給站(含鐵馬驛站)</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt11 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt11 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt11" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt11" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt11" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.車道路面是否平整</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt12 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt12 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt12" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt12" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt12" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.全線自行車(專用)道標誌/標線</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt13 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt13 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt13" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt13" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt13" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">4.是否有巡邏站</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt14 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt14 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt14" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt14" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt14" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">5.設施維護：護欄、照明、邊坡安全…等</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt15 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt15 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt15" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt15" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt15" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="category">
		<div class="categoryTitle">車道串連</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.可串接區域環線</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt16 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt16 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt16" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt16" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt16" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.可串接鄰近縣市自行車道</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt17 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt17 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt17" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt17" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt17" id="option3"autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.可串接環島線</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt18 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt18 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt18" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt18" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt18" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="category">
		<div class="categoryTitle">景點與在地特色</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.人文歷史文化景點:特殊建物、打卡標地、在地文化、古蹟文物、特色景點</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt19 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt19 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt19" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt19" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt19" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.地理景觀:特殊地形、奇特地貌、</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt20 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt20 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt20" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt20" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt20" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.自然生態: 生態公園、濕地、森林步道、水庫、動(植)物、水鳥保護區等</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt21 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt21 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt21" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt21" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt21" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">4.沿線或鄰近有在地美食及商家</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt22 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt22 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt22" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt22" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt22" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript">
function submitFunction() {
	$('#save_submit').val("submit") ;
	if ( confirm("資料送出後即無法修改")) {
		$('#form1').submit() ;
	}
}
</script>
<?php echo $footer; ?>