<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> 自行車道自我檢核表 - <font color="red"><?=$planRow['plan_name']?></font>
				</div>
				<div class="card-body">
					<?php if ($error_warning) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
					<?php } ?>

			        <?php if ($success) { ?>
			        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			        <button type="button" class="close" data-dismiss="alert">&times;</button>
			        </div>
			        <?php } ?>
					<form id="form1" method="post" action="" enctype="multipart/form-data">

<div class="checkList">
	<div class="category">
		<div class="categoryTitle">騎乘路線規劃</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.<!--2019.06.30 起訖點標示是否明確-->起訖指標是否清楚明確、無障礙標示是否明確</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt1 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt1 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt1" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt1" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt1" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
			<div class="col-md-12 col-xs-12 pt-2 d-flex flex-wrap">
				<span class="text-primary pr-2">無障礙補充說明：</span>
				<input class="flex-fill" type="text" name="acc1-1" value="<?=$acc1_1?>" size="50" style="min-width: 140px;">
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.起(終)點是否有公共交通運輸</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt2 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt2 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt2" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt2" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt2" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.沿線是否有2座以上導覽說明牌</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt3 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt3 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt3" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt3" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt3" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">4.是否有休憩點設置</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt4 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt4 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt4" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt4" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt4" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">5.<!--2019.06.30 休憩點是否有公(流)廁、自行車停車架-->休憩點是否有公(流)廁、自行車停車架、無障斜坡道、無障礙座椅席位、無障礙停車位</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt5 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt5 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt5" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt5" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt5" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
			<div class="col-md-12 col-xs-12 pt-2 d-flex flex-wrap">
				<span class="text-primary pr-2">無障礙補充說明：</span>
				<input class="flex-fill" type="text" name="acc1-5" value="<?=$acc1_5?>" size="50" style="min-width: 140px;">
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">6.<!--2019.06.30 全線告示/指示牌(雙語)是否明確:公里、方向、指示牌等等-->全線公告/指示牌(雙語)/交叉點是否清楚明確(起訖標示含公里、方向、指示牌等等)</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt6 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt6 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt6" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt6" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt6" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="category">
		<div class="categoryTitle">周邊服務</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.是否有自行車租賃站(點)</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt7 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt7 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt7" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt7" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt7" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.<!--2019.06.30 周邊是否有停車場-->周邊是否有停車場、無障礙停車位</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt8 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt8 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt8" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt8" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt8" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
			<div class="col-md-12 col-xs-12 pt-2 d-flex flex-wrap">
				<span class="text-primary pr-2">無障礙補充說明：</span>
				<input class="flex-fill" type="text" name="acc2-2" value="<?=$acc2_2?>" size="50" name="acc1-6" style="min-width: 140px;">
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.沿線是否有公(流)廁及無障礙廁所</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt9 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt9 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt9" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt9" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt9" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">4.是否有遊客中心</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt10 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt10 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt10" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt10" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt10" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="category">
		<div class="categoryTitle">路面與安全設施</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.沿線有便利商店或補給站(含鐵馬驛站)</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt11 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt11 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt11" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt11" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt11" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.<!--2019.06.30 車道路面是否平整-->車道路面是否平整、無障礙通路</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt12 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt12 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt12" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt12" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt12" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
			<div class="col-md-12 col-xs-12 pt-2 d-flex flex-wrap">
				<span class="text-primary pr-2">無障礙補充說明：</span>
				<input class="flex-fill" type="text" name="acc3-2" value="<?=$acc3_2?>" size="50" style="min-width: 140px;">
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.<!--2019.06.30 全線自行車(專用)道標誌/標線-->全線自行車(專用)道標誌/標線是否足夠、無障礙標示</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt13 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt13 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt13" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt13" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt13" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
			<div class="col-md-12 col-xs-12 pt-2 d-flex flex-wrap">
				<span class="text-primary pr-2">無障礙補充說明：</span>
				<input class="flex-fill" type="text" name="acc3-3" value="<?=$acc3_3?>" size="50" style="min-width: 140px;">
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">4.是否有巡邏站</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt14 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt14 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt14" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt14" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt14" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">5.設施維護：護欄、照明、邊坡安全…等</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt15 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt15 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt15" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt15" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt15" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="category">
		<div class="categoryTitle">車道串連</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.可串接區域環線</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt16 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt16 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt16" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt16" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt16" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.可串接鄰近縣市自行車道</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt17 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt17 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt17" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt17" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt17" id="option3"autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.可串接環島線</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt18 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt18 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt18" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt18" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt18" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="category">
		<div class="categoryTitle">景點與在地特色</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">1.人文歷史文化景點:特殊建物、打卡標地、在地文化、古蹟文物、特色景點</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt19 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt19 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt19" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt19" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt19" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">2.地理景觀:特殊地形、奇特地貌、</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt20 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt20 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt20" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt20" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt20" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">3.自然生態: 生態公園、濕地、森林步道、水庫、動(植)物、水鳥保護區等</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt21 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt21 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt21" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt21" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt21" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
		<div class="listItem row">
			<div class="col-md-8 col-xs-12">
				<label class="questions">4.沿線或鄰近有在地美食及商家</label>
			<?php
				$rCheck = array("", "", "") ;
				$aStr   = array("", "", "") ;
				if ( $opt22 == "O") {
					$rCheck[1] = "checked" ;
					$aStr[1]   = "active" ;
				} else if ( $opt22 == "X") {
					$rCheck[2] = "checked" ;
					$aStr[2]   = "active" ;
				}  else {
					$rCheck[0] = "checked" ;
					$aStr[0]   = "active" ;
				}
			?>
			</div>
			<div class="col-md-4 col-xs-12 text-right">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-light <?=$aStr[0]?>">
						<input type="radio" name="opt22" id="option1" autocomplete="off" <?=$rCheck[0]?>>未作答
					</label>
					<label class="btn btn-light <?=$aStr[1]?>">
						<input type="radio" name="opt22" id="option2" autocomplete="off" value="O" <?=$rCheck[1]?>>O
					</label>
					<label class="btn btn-light <?=$aStr[2]?>">
						<input type="radio" name="opt22" id="option3" autocomplete="off" value="X" <?=$rCheck[2]?>>X
					</label>
				</div>
			</div>
		</div>
	</div>
</div>


<input type="hidden" id="save_submit" name="save_submit" >
<?php if ($showButton) : ?>
<p>您可編輯部分內容後，按下【儲存】系統會將已編輯的內容暫存而不會送出。下次登入仍可繼續編輯或修改內容。<br>
按下【送出】後，即將內容上傳到系統，無法再做編輯修改，請確認所有內容都已編輯完畢再按送出。</p>
<button type="submit" class="btn btn-primary btn-block" >儲存</button>
<button type="button" class="btn btn-success btn-block" onclick="submitFunction()">送出</button>
<?php endif ; ?>
					</form>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript">
function submitFunction() {
	$('#save_submit').val("submit") ;
	if ( confirm("資料送出後即無法修改")) {
		$('#form1').submit() ;
	}
}
</script>
<?php echo $footer; ?>