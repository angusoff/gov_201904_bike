<?php echo $header; ?>
<div id="wrapper">
<?php echo $column_left; ?>
	<div id="content-wrapper">
		<div class="container-fluid">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					<i class="fas fa-table"></i> 自行車道名單
				</div>
				<div class="card-body">
					<div class="table-responsive">
	<p>註:</p>
	<ol>
		<li>問卷表答覆請盡可能詳細敘述，使專家學者能加速了解車道狀況。</li>
		<li>請於收文後依指定網址及日期線上填寫完成，以利後續輔導計畫進度。</li>
		<li>填寫若有任何問題，請洽聯網國際資訊股份有限公司 何小姐 02-87871315 #301</li>
	</ol>

						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>區域</th>
									<th>計畫名稱</th>
									<th>填報</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($planList as $iCnt => $tmp) : ?>
								<tr>
									<td><?=$tmp['area']?></td>
									<td><?=$tmp['plan_name']?></td>
									<td>
										<a class="btn <?=$tmp['btCss']?> btn-block" href="<?=$tmp['href']?>"><?=$tmp['btName']?></a>

										<?php //if ( empty( $tmp['organ'])) : ?>
										<!--<a class="btn btn-primary btn-block" href="<?=$tmp['href']?>">填報</a>-->
										<?php //else : ?>
										<!--<a class="btn btn-success btn-block" href="<?=$tmp['href']?>">自評表</a>-->
										<?php //endif ; ?>
									</td>
								</tr>
								<?php endforeach ; ?>
							</tbody>
						</table>


					</div>
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content-wrapper -->
</div>
<!-- /#wrapper -->
<?php echo $footer; ?>