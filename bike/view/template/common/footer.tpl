		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top">
			<i class="fas fa-angle-up"></i>
		</a>

		<!-- Bootstrap core JavaScript-->
		<script src="bike/view/vendor/jquery/jquery.min.js"></script>
		<script src="bike/view/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<!-- Core plugin JavaScript-->
		<script src="bike/view/vendor/jquery-easing/jquery.easing.min.js"></script>
		<!-- Page level plugin JavaScript-->
		<script src="bike/view/vendor/datatables/jquery.dataTables.js"></script>
		<script src="bike/view/vendor/datatables/dataTables.bootstrap4.js"></script>
		<!-- Custom scripts for all pages-->
		<script src="bike/view/js/sb-admin.js"></script>
		<!-- Demo scripts for this page-->
		<script src="bike/view/js/demo/datatables-demo.js"></script>
	</body>
</html>