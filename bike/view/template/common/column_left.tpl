<!-- Sidebar -->
<ul class="sidebar navbar-nav">
	<?php if ( $showJudge) : ?>
	<div id="profile" class="text-white p-3">
		<div>
			<h5> <i class="fa fa-user fa-fw"></i> <?=$loginName?></h5>
			<small><a class="nav-link" href="?route=bike/judge/logout">登出</a></small>
		</div>
	</div>
	<?php endif ; ?>

	<?php if ( $loginHandle) : ?>
	<li class="nav-item active">
		<a class="nav-link" href="./">
			<i class="fas fa-fw fa-table"></i>
				<span>自行車道名單</span></a>
	</li>
	<?php endif ; ?>
	<?php if ( $loginJudge) : ?>
	<li class="nav-item active">
		<a class="nav-link" href="?route=bike/judge">
			<i class="fas fa-fw fa-table"></i>
				<span>委員評分</span></a>
	</li>
	<?php endif ; ?>
	<?php if ( $showHandle) : ?>
	<li class="nav-item">
		<a class="nav-link" href="?route=common/dashboard/self_evaluationForm">
			<i class="fas fa-fw fa-table"></i>
				<span>自我評量表</span></a>
	</li>
	<li class="nav-item">
		<a class="nav-link" href="?route=common/dashboard/self_checkForm">
			<i class="fas fa-fw fa-table"></i>
				<span>自我檢核表</span></a>
	</li>
	<?php elseif ( $showJudge) : ?>
	<li class="nav-item">
		<a class="nav-link" href="?route=bike/judge/showscore">
			<i class="fas fa-fw fa-table"></i>
				<span>委員評分結果</span></a>
	</li>
	<?php endif ; ?>
</ul>
<!-- Sidebar End -->