<?php
class ControllerBikeJob extends Controller {
	public function index() {
		$this->load->model( 'bike/job') ;
		$retArr = $this->model_bike_job->getJudgeAnswer() ;
		// dump( $retArr) ;
		$rank = $this->buileRank( $retArr) ;
		// dump( $rank) ;
		foreach ($retArr as $planID => $judgeArr) {
			$ansOptArr = array() ;
			$retOptArr = array() ;
			foreach ($judgeArr as $judgeID => $answer) {
				$optArr = json_decode( $answer['answer'], true)	;
				$ansOptArr[$judgeID] = $optArr ;
				$retOptArr[$judgeID] = $this->totalCnt( $optArr) ;
			}
			$area_name = $judgeArr[$judgeID]['area'] ;
			$plan_name = $judgeArr[$judgeID]['plan_name'] ;


			echo "案號 : {$planID}\n" ;
			echo "地區 : {$area_name}\n" ;
			echo "{$plan_name}\n" ;
			echo "項目,各項目權重,委員A,委員B,委員C\n" ;
			echo "一、騎乘路線規劃,25%,{$retOptArr['a0001']['1']},{$retOptArr['a0002']['1']},{$retOptArr['a0010']['1']}\n" ;
			echo "1.起訖點標示是否明確,20%,{$ansOptArr['a0001']['aopt1']},{$ansOptArr['a0002']['aopt1']},{$ansOptArr['a0010']['aopt1']}\n" ;
			echo "2.起(終)點是否有公共交通運輸,10%,{$ansOptArr['a0001']['aopt2']},{$ansOptArr['a0002']['aopt2']},{$ansOptArr['a0010']['aopt2']}\n" ;
			echo "3.沿線是否有2座以上導覽說明牌,20%,{$ansOptArr['a0001']['aopt3']},{$ansOptArr['a0002']['aopt3']},{$ansOptArr['a0010']['aopt3']}\n" ;
			echo "4.是否有休憩點設置,10%,{$ansOptArr['a0001']['aopt4']},{$ansOptArr['a0002']['aopt4']},{$ansOptArr['a0010']['aopt4']}\n" ;
			echo "5.休憩點是否有公(流)廁、自行車停車架,15%,{$ansOptArr['a0001']['aopt5']},{$ansOptArr['a0002']['aopt5']},{$ansOptArr['a0010']['aopt5']}\n" ;
			echo "6.全線告示/指示牌(雙語)是否明確:公里、方向、指示牌等等,25%,{$ansOptArr['a0001']['aopt6']},{$ansOptArr['a0002']['aopt6']},{$ansOptArr['a0010']['aopt6']}\n\n" ;

			echo "二、周邊服務,10%,{$retOptArr['a0001']['2']},{$retOptArr['a0002']['2']},{$retOptArr['a0010']['2']}\n" ;
			echo "1.是否有自行車租賃站(點),20%,{$ansOptArr['a0001']['aopt7']},{$ansOptArr['a0002']['aopt7']},{$ansOptArr['a0010']['aopt7']}\n" ;
			echo "2.周邊是否有停車場,20%,{$ansOptArr['a0001']['aopt8']},{$ansOptArr['a0002']['aopt8']},{$ansOptArr['a0010']['aopt8']}\n" ;
			echo "3.沿線是否有公(流)廁及無障礙廁所,40%,{$ansOptArr['a0001']['aopt9']},{$ansOptArr['a0002']['aopt9']},{$ansOptArr['a0010']['aopt9']}\n" ;
			echo "4.是否有遊客中心,20%,{$ansOptArr['a0001']['aopt10']},{$ansOptArr['a0002']['aopt10']},{$ansOptArr['a0010']['aopt10']}\n\n" ;

			echo "三、路面與安全設施,25%,{$retOptArr['a0001']['3']},{$retOptArr['a0002']['3']},{$retOptArr['a0010']['3']}\n" ;
			echo "1.沿線有便利商店或補給站(含鐵馬驛站),15%,{$ansOptArr['a0001']['aopt11']},{$ansOptArr['a0002']['aopt11']},{$ansOptArr['a0010']['aopt11']}\n" ;
			echo "2.車道路面是否平整,25%,{$ansOptArr['a0001']['aopt12']},{$ansOptArr['a0002']['aopt12']},{$ansOptArr['a0010']['aopt12']}\n" ;
			echo "3.全線自行車(專用)道標誌/標線,25%,{$ansOptArr['a0001']['aopt13']},{$ansOptArr['a0002']['aopt13']},{$ansOptArr['a0010']['aopt13']}\n" ;
			echo "4.是否有巡邏站,15%,{$ansOptArr['a0001']['aopt14']},{$ansOptArr['a0002']['aopt14']},{$ansOptArr['a0010']['aopt14']}\n" ;
			echo "5.設施維護：護欄、照明、邊坡安全…等,20%,{$ansOptArr['a0001']['aopt15']},{$ansOptArr['a0002']['aopt15']},{$ansOptArr['a0010']['aopt15']}\n\n" ;

			echo "四、車道串連,20%,{$retOptArr['a0001']['4']},{$retOptArr['a0002']['4']},{$retOptArr['a0010']['4']}\n" ;
			echo "1.可串接區域環線,50%,{$ansOptArr['a0001']['aopt16']},{$ansOptArr['a0002']['aopt16']},{$ansOptArr['a0010']['aopt16']}\n" ;
			echo "2.可串接鄰近縣市自行車道,25%,{$ansOptArr['a0001']['aopt17']},{$ansOptArr['a0002']['aopt17']},{$ansOptArr['a0010']['aopt17']}\n" ;
			echo "3.可串接環島線,25%,{$ansOptArr['a0001']['aopt18']},{$ansOptArr['a0002']['aopt18']},{$ansOptArr['a0010']['aopt18']}\n\n" ;

			echo "五、景點與在地特色,20%,{$retOptArr['a0001']['5']},{$retOptArr['a0002']['5']},{$retOptArr['a0010']['5']}\n" ;
			echo "1.人文歷史文化景點:特殊建物、打卡標地、在地文化、古蹟文物、特色景點,25%,{$ansOptArr['a0001']['aopt19']},{$ansOptArr['a0002']['aopt19']},{$ansOptArr['a0010']['aopt19']}\n" ;
			echo "2.地理景觀:特殊地形、奇特地貌,25%,{$ansOptArr['a0001']['aopt20']},{$ansOptArr['a0002']['aopt20']},{$ansOptArr['a0010']['aopt20']}\n" ;
			echo "3.自然生態: 生態公園、濕地、森林步道、水庫、動(植)物、水鳥保護區等,25%,{$ansOptArr['a0001']['aopt21']},{$ansOptArr['a0002']['aopt21']},{$ansOptArr['a0010']['aopt21']}\n" ;
			echo "4.沿線或鄰近有在地美食及商家,25%,{$ansOptArr['a0001']['aopt22']},{$ansOptArr['a0002']['aopt22']},{$ansOptArr['a0010']['aopt22']}\n\n" ;

			echo "合計總分,,{$retOptArr['a0001']['0']} / {$rank[0]['a0001'][$planID]},{$retOptArr['a0002']['0']} / {$rank[0]['a0002'][$planID]},{$retOptArr['a0010']['0']} / {$rank[0]['a0010'][$planID]}\n" ;
			echo "總排名 : {$rank[1][$planID]}\n\n\n" ;
			echo "評審委員簽名 : \n" ;
			echo "\n\n\n\n\n\n\n\n";

		}
	}


	protected function buileRank( $data = array()) {
		$judgeScore    = array() ;
		$planJudgeRank = array() ;

		foreach ($data as $planID => $judgeAns) {
			foreach ($judgeAns as $judgeID => $ansArr) {
				$judgeScore[$judgeID][$planID] = $ansArr['total'] ;
			}
		}

		foreach ( $judgeScore as $judgeID => $value) {
			arsort( $value) ;
			$judgeScore[$judgeID] = $value ;
			$rank = 1 ;
			foreach ($value as $planID => $tmp) {
				$planJudgeRank[$judgeID][$planID] = $rank ;
				$rank ++ ;
			}
		}

		$ordinal = array() ;
		foreach ($planJudgeRank as $judgeID => $rankArr) {
			foreach ($rankArr as $planID => $rank) {
				if ( !isset( $ordinal[$planID])) {
					$ordinal[$planID] = 0 ;
				}
				if ( $rank <=5) {

					$ordinal[$planID] += 1 ;
				} else if ($rank >=6 && $rank <= 10) {
					$ordinal[$planID] += 2 ;
				} else if ($rank >=11 && $rank <= 15) {
					$ordinal[$planID] += 3 ;
				} else if ($rank >=16 && $rank <= 20) {
					$ordinal[$planID] += 4 ;
				} else if ($rank >=21 && $rank <= 25) {
					$ordinal[$planID] += 5 ;
				} else if ($rank >=26 && $rank <= 30) {
					$ordinal[$planID] += 6 ;
				} else if ($rank >=31 && $rank <= 35) {
					$ordinal[$planID] += 7 ;
				} else if ($rank >=36 && $rank <= 40) {
					$ordinal[$planID] += 8 ;
				} else if ($rank >=41 && $rank <= 45) {
					$ordinal[$planID] += 9 ;
				} else if ($rank >=46 && $rank <= 50) {
					$ordinal[$planID] += 10 ;
				}
			}
		}
		$ordinal = $this->rankBuild( $ordinal) ;

		// dump( $ordinal) ;
		// dump( $planJudgeRank) ;

		return array( $planJudgeRank, $ordinal) ;
	}

	protected function rankBuild( $data = array()) {
		// dump( $data) ;
		// 分數排序
		$scoreArr = array() ;
		foreach ($data as $planID => $score) {
			$scoreArr[$score][] = $planID ;
		}
		ksort( $scoreArr) ;
		// dump( $scoreArr) ;

		// 名次排序
		$rank = 1 ;
		foreach ($scoreArr as $score => $tmpArr) {
			sort( $tmpArr) ;
			foreach ($tmpArr as $iCnt => $planID) {
				$rankArr[$planID] = $rank ;
				$rank ++ ;
			}
		}

		// dump( $rankArr) ;
		return $rankArr ;
	}

	/**
	 * [totalCnt 評委評分後 計算總得分 並回存資料庫]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-04-23
	 */
	protected function totalCnt( $data = array()) {
		$retTotal = 0 ;
		$classCnt = 0 ;
		$area1 = 0 ;
		$area2 = 0 ;
		$area3 = 0 ;
		$area4 = 0 ;
		$area5 = 0 ;

		// echo $data['aopt1'] * 0.2 ."<br>";
		// echo $data['aopt2'] * 0.1 ."<br>";
		// echo $data['aopt3'] * 0.2 ."<br>";
		// echo $data['aopt4'] * 0.1 ."<br>";
		// echo $data['aopt5'] * 0.15 ."<br>";
		// echo $data['aopt6'] * 0.25 ."<br>";

		$classCnt += isset( $data['aopt1']) ? $data['aopt1'] * 0.2 : 0 ;
		$classCnt += isset( $data['aopt2']) ? $data['aopt2'] * 0.1 : 0 ;
		$classCnt += isset( $data['aopt3']) ? $data['aopt3'] * 0.2 : 0 ;
		$classCnt += isset( $data['aopt4']) ? $data['aopt4'] * 0.1 : 0 ;
		$classCnt += isset( $data['aopt5']) ? $data['aopt5'] * 0.15 : 0 ;
		$classCnt += isset( $data['aopt6']) ? $data['aopt6'] * 0.25 : 0 ;
		// echo $classCnt."<br>";
		$area1    = $classCnt;
		$retTotal += $area1 * 0.25 ;
		$classCnt = 0 ;

		$classCnt += isset( $data['aopt7']) ? $data['aopt7'] * 0.2 : 0 ;
		$classCnt += isset( $data['aopt8']) ? $data['aopt8'] * 0.2 : 0 ;
		$classCnt += isset( $data['aopt9']) ? $data['aopt9'] * 0.4 : 0 ;
		$classCnt += isset( $data['aopt10']) ? $data['aopt10'] * 0.2 : 0 ;
		$area2    = $classCnt ;
		$retTotal += $area2 * 0.1 ;
		$classCnt = 0 ;

		$classCnt += isset( $data['aopt11']) ? $data['aopt11'] * 0.15 : 0 ;
		$classCnt += isset( $data['aopt12']) ? $data['aopt12'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt13']) ? $data['aopt13'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt14']) ? $data['aopt14'] * 0.15 : 0 ;
		$classCnt += isset( $data['aopt15']) ? $data['aopt15'] * 0.2 : 0 ;
		$area3    = $classCnt ;
		$retTotal += $area3 * 0.25 ;
		$classCnt = 0 ;

		$classCnt += isset( $data['aopt16']) ? $data['aopt16'] * 0.5 : 0 ;
		$classCnt += isset( $data['aopt17']) ? $data['aopt17'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt18']) ? $data['aopt18'] * 0.25 : 0 ;
		$area4    = $classCnt ;
		$retTotal += $area4 * 0.2 ;
		$classCnt = 0 ;

		$classCnt += isset( $data['aopt19']) ? $data['aopt19'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt20']) ? $data['aopt20'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt21']) ? $data['aopt21'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt22']) ? $data['aopt22'] * 0.25 : 0 ;
		$area5    = $classCnt;
		$retTotal += $area5 * 0.2;
		$classCnt = 0 ;

		// dump( $retTotal) ;
		return array( round($retTotal, 2), round( $area1, 2), round( $area2, 2), round( $area3, 2), round( $area4, 2), round( $area5, 2)) ;
	}
}