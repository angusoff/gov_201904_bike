<?php
class ControllerBikeJudge extends Controller {
	private $error  = array() ;
	private $judger = array() ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-28
	 */
	public function index() {

		$this->load->model( 'bike/judge') ;
		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm()) {
			if ( isset( $this->judger)) {
				$filter_data = array(
					"account"  => $this->judger->row['account'],
				) ;
				$this->model_bike_judge->insertJudgerLog( $filter_data) ;
				$this->session->data['judgeID']   = $this->judger->row['account'] ;
				$this->session->data['judgeName'] = $this->judger->row['name'] ;

				$this->response->redirect($this->url->link('bike/judge/showList', "", true)) ;
			}
		}

		// 已登入
		if ( isset( $this->session->data['judgeID']) && isset( $this->session->data['judgeName'])) {
			$this->response->redirect($this->url->link('bike/judge/showList', "", true)) ;
		}

		// 訊息
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
			foreach ( $this->error as $errKey => $tmpStr) {
				if ( $errKey != "warning") $data['error_warning'] .= "<br>{$tmpStr}" ;
			}
		} else {
			$data['error_warning'] = '' ;
		}

		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('judge/conform', $data));
	}

	/**
	 * [showList 登入後的列表頁]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-28
	 */
	public function showList() {
		$this->isLogin() ;
		$this->load->model( 'bike/judge') ;
		$this->load->model( 'bike/report') ;

		// 接收排序條件
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'idx';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$url = '';
		// 欄位用的排序條件
		if ($order == 'ASC') {
			$url                   .= '&order=DESC';
			$data['url_sort_icon'] = "fa-sort-up" ;
		} else {
			$url                   .= '&order=ASC';
			$data['url_sort_icon'] = "fa-sort-down" ;
		}

		$data['sort_area'] = $this->url->link('bike/judge/showList', '&sort=idx' . $url, true);
		$data['sort_grade'] = $this->url->link('bike/judge/showList', '&sort=grade' . $url, true);

		$data['sort'] = $sort;
		$data['order'] = $order;
		// dump( $data) ;

		// 訊息
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
			foreach ( $this->error as $errKey => $tmpStr) {
				if ( $errKey != "warning") $data['error_warning'] .= "<br>{$tmpStr}" ;
			}
		} else {
			$data['error_warning'] = '' ;
		}

		if ( isset( $this->session->data['success'])) {
			$data['success'] = $this->session->data['success'] ;
			unset( $this->session->data['success']) ;
		} else {
			$data['success'] = '' ;
		}

		// 查詢條件
		$filter_data = array(
			'sort'       => $sort,
			'order'      => $order,
			"judge_user" => $this->session->data['judgeID'],
		) ;
		$data['planList'] = $this->model_bike_judge->getPlanLists( $filter_data) ;
		$planInfo = $this->model_bike_report->getPlanAnsStatus() ;

		// dump( $planInfo) ;

		foreach ( $data['planList'] as $iCnt => $tmp) {
			// if ( isset( $planInfo[$tmp['idx']]) && isset( $planInfo[$tmp['idx']]['evaluation']) && isset( $planInfo[$tmp['idx']]['checking'])) {
				$url = "bike/judge/planInfo" ;
				$v1 = isset( $planInfo[$tmp['idx']]['evaluation']) ? $planInfo[$tmp['idx']]['evaluation'] : "" ;
				$v2 = isset( $planInfo[$tmp['idx']]['checking']) ? $planInfo[$tmp['idx']]['checking'] : "" ;
				$url = $this->url->link('bike/judge/planInfo', "pid={$tmp['idx']}&doc_num={$tmp['doc_num']}&v1={$v1}&v2={$v2}", true) ;
				$data['planList'][$iCnt]['viewButton'] = '<a class="btn btn-secondary" href="'. $url . '"><i class="fa fa-search fa-fw"></i></a>' ;

			// } else {
			// 	$data['planList'][$iCnt]['viewButton'] = "" ;
			// }
		}

		// 欄位用的排序條件
		if ($order == 'ASC') {
			$url = '&sort=DESC';
			$url_sort_icon = "fa-sort-up" ;
		} else {
			$url = '&sort=ASC';
			$url_sort_icon = "fa-sort-down" ;
		}

		$data['url_sort']      = $this->url->link('bike/judge/showList', $url, true) ;
		$data['url_sort_icon'] = $url_sort_icon ;



		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('judge/planList', $data));
	}

	public function planInfo() {
		$this->isLogin() ;
		$this->load->model( 'bike/report') ;
		$this->load->model( 'bike/judge') ;
		// dump( $this->request->post) ;

		if ( $this->request->server['REQUEST_METHOD'] == 'POST') {
			// dump( $this->session->data) ;
			// dump( $this->request->post) ;
			$keyStr = "aopt" ;
			$answerArr = array() ;
			for ($i=1; $i < 23; $i++) {
				$ansKey = $keyStr.$i ;
				$answerArr[$ansKey] = $this->request->post[$ansKey] ;
			}

			$total = $this->totalCnt( $answerArr) ;
			$insArr = array(
				"pid"         => $this->request->post['pid'],
				"judge_user"  => $this->session->data['judgeID'],
				"answer"      => $answerArr,
				"total"       => $total[0],
				"save_submit" => $this->request->post['save_submit'],
			) ;

			$this->model_bike_judge->saveJudgeAns( $insArr) ;
			$this->session->data['success'] = "填報資料 儲存成功";

			$this->response->redirect($this->url->link('bike/judge/showList', "", true)) ;
		}


		// 訊息
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
			foreach ( $this->error as $errKey => $tmpStr) {
				if ( $errKey != "warning") $data['error_warning'] .= "<br>{$tmpStr}" ;
			}
		} else {
			$data['error_warning'] = '' ;
		}


		$filter_data = array (
			"idx"     => $this->request->get['pid'],
			"doc_num" => $this->request->get['doc_num'],
		) ;
		$data['planRow'] = $this->model_bike_report->getPlanName( $filter_data) ;
		$isReport = $this->model_bike_report->getAnswer( $filter_data) ;
		// dump( $isReport) ;

		if ( !empty( $isReport->row['answer'])) {
			$items = json_decode( $isReport->row['answer']) ;

			foreach ($items as $keyName => $value) {
				$data[$keyName] = isset( $value) ? $value : "" ;
			}
		} else {
			for( $i = 1 ; $i < 16 ; $i++) {
				$keyName = "item".$i ;
				$data[$keyName] = "" ;
			}
		}

		$data['startRoute']  = isset( $isReport->row['startRoute']) ? $isReport->row['startRoute'] : "" ;
		$data['plan']        = isset( $isReport->row['plan']) ? $isReport->row['plan'] : "" ;
		$data['planDate1']   = isset( $isReport->row['planDate1']) ? $isReport->row['planDate1'] : "" ;
		$data['planDate2']   = isset( $isReport->row['planDate2']) ? $isReport->row['planDate2'] : "" ;
		$data['planDate3']   = isset( $isReport->row['planDate3']) ? $isReport->row['planDate3'] : "" ;
		$data['planDate4']   = isset( $isReport->row['planDate4']) ? $isReport->row['planDate4'] : "" ;

		$data['planStatus']  = isset( $isReport->row['planStatus']) ? $isReport->row['planStatus'] : "" ;
		$data['environment'] = isset( $isReport->row['environment']) ? $isReport->row['environment'] : "" ;
		$data['route']       = isset( $isReport->row['route']) ? $isReport->row['route'] : "" ;

		$isReport = $this->model_bike_report->getSelfAnswer( $filter_data) ;
		// dump( $isReport) ;
		if ( !empty( $isReport->row['answer'])) {
			$items = json_decode( $isReport->row['answer']) ;
			foreach ($items as $keyName => $value) {
				if ( isset( $value)) {
					switch ($value) {
						case 'O':
							$data[$keyName] = "<button type=\"button\" class=\"btn btn-primary btn-sm\" disable>O</button>" ;
							break;
						case 'X':
							$data[$keyName] = "<button type=\"button\" class=\"btn btn-danger btn-sm\" disable>X</button>" ;
							break;

						default:
							$data[$keyName] = "" ;
							break;
					}
				} else {
					$data[$keyName] = "" ;
				}
			}
		} else {
			for( $i = 1 ; $i < 23 ; $i++) {
				$keyName = "opt".$i ;
				$data[$keyName] = "" ;
			}
		}
		$data['acc1_1'] = isset( $isReport->row['acc1_1']) ? $isReport->row['acc1_1'] : "" ;
		$data['acc1_5'] = isset( $isReport->row['acc1_5']) ? $isReport->row['acc1_5'] : "" ;
		$data['acc2_2'] = isset( $isReport->row['acc2_2']) ? $isReport->row['acc2_2'] : "" ;
		$data['acc3_2'] = isset( $isReport->row['acc3_2']) ? $isReport->row['acc3_2'] : "" ;
		$data['acc3_3'] = isset( $isReport->row['acc3_3']) ? $isReport->row['acc3_3'] : "" ;

		// 取答案
		$filter_data['judgeID'] = $this->session->data['judgeID'] ;

		$ansArr = $this->model_bike_judge->getJudgeAnswer( $filter_data) ;
		if ( $ansArr->num_rows == 1) {
			$items = json_decode( $ansArr->row['answer']) ;
			foreach ($items as $keyName => $value) {
				$data[$keyName] = isset( $value) ? $value : "" ;
			}
		} else {
			$keyStr = "aopt" ;
			for ($i=1; $i < 23; $i++) {
				$keyName = $keyStr.$i ;
				$data[$keyName] = "" ;
			}
		}
		// dump( $data) ;
		$data['pid'] = $this->request->get['pid'] ;


		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('judge/planInfo', $data));
	}

	public function planInfoView() {
		$this->load->model( 'bike/report') ;
		$this->load->model( 'bike/judge') ;

		$filter_data = array (
			"idx"     => $this->request->get['pid'],
			"doc_num" => $this->request->get['doc_num'],
		) ;
		$data['planRow'] = $this->model_bike_report->getPlanName( $filter_data) ;
		$isReport = $this->model_bike_report->getAnswer( $filter_data) ;
		// dump( $isReport) ;

		if ( !empty( $isReport->row['answer'])) {
			$items = json_decode( $isReport->row['answer']) ;
			// dump( $items) ;
			foreach ($items as $keyName => $value) {
				$data[$keyName] = isset( $value) ? $value : "" ;
			}
		} else {
			for( $i = 1 ; $i < 16 ; $i++) {
				$keyName = "item".$i ;
				$data[$keyName] = "" ;
			}
		}

		$data['startRoute']  = isset( $isReport->row['startRoute']) ? $isReport->row['startRoute'] : "" ;
		$data['plan']        = isset( $isReport->row['plan']) ? $isReport->row['plan'] : "" ;
		$data['planDate1']   = isset( $isReport->row['planDate1']) ? $isReport->row['planDate1'] : "" ;
		$data['planDate2']   = isset( $isReport->row['planDate2']) ? $isReport->row['planDate2'] : "" ;
		$data['planDate3']   = isset( $isReport->row['planDate3']) ? $isReport->row['planDate3'] : "" ;
		$data['planDate4']   = isset( $isReport->row['planDate4']) ? $isReport->row['planDate4'] : "" ;

		$data['planStatus']  = isset( $isReport->row['planStatus']) ? $isReport->row['planStatus'] : "" ;
		$data['environment'] = isset( $isReport->row['environment']) ? $isReport->row['environment'] : "" ;
		$data['route']       = isset( $isReport->row['route']) ? $isReport->row['route'] : "" ;

		$isReport = $this->model_bike_report->getSelfAnswer( $filter_data) ;
		// dump( $isReport) ;
		if ( !empty( $isReport->row['answer'])) {
			$items = json_decode( $isReport->row['answer']) ;
			foreach ($items as $keyName => $value) {
				if ( isset( $value)) {
					switch ($value) {
						case 'O':
							$data[$keyName] = "<button type=\"button\" class=\"btn btn-primary btn-sm\" disable>O</button>" ;
							break;
						case 'X':
							$data[$keyName] = "<button type=\"button\" class=\"btn btn-danger btn-sm\" disable>X</button>" ;
							break;

						default:
							$data[$keyName] = "" ;
							break;
					}
				} else {
					$data[$keyName] = "" ;
				}
			}
		} else {
			for( $i = 1 ; $i < 23 ; $i++) {
				$keyName = "opt".$i ;
				$data[$keyName] = "" ;
			}
		}
		$data['acc1_1'] = isset( $isReport->row['acc1_1']) ? $isReport->row['acc1_1'] : "" ;
		$data['acc1_5'] = isset( $isReport->row['acc1_5']) ? $isReport->row['acc1_5'] : "" ;
		$data['acc2_2'] = isset( $isReport->row['acc2_2']) ? $isReport->row['acc2_2'] : "" ;
		$data['acc3_2'] = isset( $isReport->row['acc3_2']) ? $isReport->row['acc3_2'] : "" ;
		$data['acc3_3'] = isset( $isReport->row['acc3_3']) ? $isReport->row['acc3_3'] : "" ;

		// 取答案
		// $filter_data['judgeID'] = $this->session->data['judgeID'] ;

		// $ansArr = $this->model_bike_judge->getJudgeAnswer( $filter_data) ;
		// if ( $ansArr->num_rows == 1) {
		// 	$items = json_decode( $ansArr->row['answer']) ;
		// 	foreach ($items as $keyName => $value) {
		// 		$data[$keyName] = isset( $value) ? $value : "" ;
		// 	}
		// } else {
		// 	$keyStr = "aopt" ;
		// 	for ($i=1; $i < 23; $i++) {
		// 		$keyName = $keyStr.$i ;
		// 		$data[$keyName] = "" ;
		// 	}
		// }
		// dump( $data) ;
		$data['pid'] = $this->request->get['pid'] ;


		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('judge/planInfoView', $data));
	}


	/**
	 * [showscore 評委填分進度]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-28
	 */
	public function showscore() {
		$this->load->model( 'bike/judge') ;
		$this->load->model( 'bike/report') ;

		// 查詢條件
		// 接收排序條件
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'idx';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$url = '';
		// 欄位用的排序條件
		if ($order == 'ASC') {
			$url                   .= '&order=DESC';
			$data['url_sort_icon'] = "fa-sort-up" ;
		} else {
			$url                   .= '&order=ASC';
			$data['url_sort_icon'] = "fa-sort-down" ;
		}

		$data['sort_area'] = $this->url->link('bike/judge/showscore', '&sort=idx' . $url, true) ;
		$data['sort_rank'] = $this->url->link('bike/judge/showscore', '&sort=rank' . $url, true) ;

		$data['sort'] = $sort ;
		$data['order'] = $order ;


		$data['planList']   = $this->model_bike_report->getPlanLists() ;
		// dump( $data['planList']) ;
		$planList = array() ;
		foreach ($data['planList'] as $iCnt => $tmp) {
			$planList[$tmp['idx']] = $tmp ;
		}
		$data['planList'] = $planList ;
		// dump( $data['planList']) ;
		$data['judger']     = $this->model_bike_judge->getJudger() ;
		// dump( $data['judger']) ;
		$data['judgeScore'] = $this->model_bike_judge->getJudgeScore() ;
		$ordinal = array() ;
		foreach ( $data['judgeScore'] as $judgeID => $scoreArr) {
			$rank = 1 ;
			foreach ( $scoreArr as $planID => $score) {
				if ( !isset( $ordinal[$planID])) {
					$ordinal[$planID] = 0 ;
				}
				if ( $rank <=5) {
					$ordinal[$planID] += 1 ;
				} else if ($rank >=6 && $rank <= 10) {
					$ordinal[$planID] += 2 ;
				} else if ($rank >=11 && $rank <= 15) {
					$ordinal[$planID] += 3 ;
				} else if ($rank >=16 && $rank <= 20) {
					$ordinal[$planID] += 4 ;
				} else if ($rank >=21 && $rank <= 25) {
					$ordinal[$planID] += 5 ;
				} else if ($rank >=26 && $rank <= 30) {
					$ordinal[$planID] += 6 ;
				} else if ($rank >=31 && $rank <= 35) {
					$ordinal[$planID] += 7 ;
				} else if ($rank >=36 && $rank <= 40) {
					$ordinal[$planID] += 8 ;
				} else if ($rank >=41 && $rank <= 45) {
					$ordinal[$planID] += 9 ;
				} else if ($rank >=46 && $rank <= 50) {
					$ordinal[$planID] += 10 ;
				}
				$data['judgeScore'][$judgeID][$planID] = $score . " / " . $rank ;
				$rank ++ ;
			}
		}
		$data['ordinal'] = $ordinal ;
		$data['rankArr'] = $this->rankBuild( $ordinal) ;

		// dump( $data['rankArr']) ;
		// dump( $data['ordinal']) ;
		// dump( $data['judgeScore']) ;

		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		if ( $sort == 'rank') {
			if ( $order == 'DESC') {
				arsort( $data['rankArr']) ;
			}
			$this->response->setOutput($this->load->view('judge/judgeScore_sort_rank', $data));
		} else {
			if ( $order == 'DESC') {
				arsort( $data['planList']) ;
			}
			$this->response->setOutput($this->load->view('judge/judgeScore', $data));
		}

	}

	/**
	 * [spiderweb description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-30
	 */
	public function spiderweb() {
		$this->load->model( 'bike/judge') ;
		$planID = isset( $this->request->get['pid']) ? $this->request->get['pid'] : "" ;

		if ( !empty( $planID)) {
			$filter_data = array(
				"pid" => $planID,
			) ;
			$planInfo = $this->model_bike_judge->getPlanName( $filter_data) ;
			$data['planInfo'] = $planInfo ;
			// dump( $planInfo) ;
			$spiderScore = $this->getAllPlanScore( $planID) ;
			$seriesStr = $this->biludJsString( $spiderScore) ;
			$data['seriesStr'] = $seriesStr ;
			// dump( $spiderScore) ;

// {
// name: 'Allocated Budget',
// data: [43000, 19000, 60000, 35000, 17000],
// pointPlacement: 'on'
// }, {
// name: 'Actual Spending',
// data: [50000, 39000, 42000, 31000, 26000],
// pointPlacement: 'on'
// }


			$data['header']         = $this->load->controller('common/header');
			$data['column_left']    = $this->load->controller('common/column_left');
			$data['footer']         = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('judge/judgeSpiderweb2', $data));
		} else {

		}
	}

	/**
	 * [logout description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-20
	 */
	public function logout() {
		unset( $this->session->data['judgeID']) ;
		unset( $this->session->data['judgeName']) ;

		$this->response->redirect($this->url->link('bike/judge', "", true)) ;
	}

	/**
	 * [isLogin description]
	 * @return  boolean    [description]
	 * @Another Angus
	 * @date    2019-04-20
	 */
	protected function isLogin() {
		if ( !isset( $this->session->data['judgeID']) && !isset( $this->session->data['judgeName'])) {
			$this->response->redirect($this->url->link('bike/judge', "", true)) ;
		}
	}

	/**
	 * [validateForm 檢查登入動作]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-23
	 */
	protected function validateForm () {
		$this->load->model( 'bike/judge') ;

		// if ( empty( $this->request->post['account']) && empty( $this->request->post['password'])) {
		// 	$this->error['warning'] = "帳號密碼未填寫" ;
		// }
		$filter_data = array (
			"account"  => $this->request->post['account'],
			"password" => $this->request->post['password'],
		) ;

		$retObj = $this->model_bike_judge->checkJudgeAcc( $filter_data) ;
		if ( $retObj->num_rows != 1) {
			$this->error['warning'] = "帳號密碼有誤 請查明後再登入" ;
		} else {
			$this->judger = $retObj ;
		}

		return !$this->error;
	}

	/**
	 * [totalCnt 評委評分後 計算總得分 並回存資料庫]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-04-23
	 */
	protected function totalCnt( $data = array()) {
		$retTotal = 0 ;
		$classCnt = 0 ;
		$area1 = 0 ;
		$area2 = 0 ;
		$area3 = 0 ;
		$area4 = 0 ;
		$area5 = 0 ;

		$classCnt += isset( $data['aopt1']) ? $data['aopt1'] * 0.2 : 0 ;
		$classCnt += isset( $data['aopt2']) ? $data['aopt2'] * 0.1 : 0 ;
		$classCnt += isset( $data['aopt3']) ? $data['aopt3'] * 0.2 : 0 ;
		$classCnt += isset( $data['aopt4']) ? $data['aopt4'] * 0.1 : 0 ;
		$classCnt += isset( $data['aopt5']) ? $data['aopt5'] * 0.15 : 0 ;
		$classCnt += isset( $data['aopt6']) ? $data['aopt6'] * 0.25 : 0 ;
		$area1    = $classCnt;
		$retTotal += $area1 * 0.25 ;
		$classCnt = 0 ;

		$classCnt += isset( $data['aopt7']) ? $data['aopt7'] * 0.2 : 0 ;
		$classCnt += isset( $data['aopt8']) ? $data['aopt8'] * 0.2 : 0 ;
		$classCnt += isset( $data['aopt9']) ? $data['aopt9'] * 0.4 : 0 ;
		$classCnt += isset( $data['aopt10']) ? $data['aopt10'] * 0.2 : 0 ;
		$area2    = $classCnt ;
		$retTotal += $area2 * 0.1 ;
		$classCnt = 0 ;

		$classCnt += isset( $data['aopt11']) ? $data['aopt11'] * 0.15 : 0 ;
		$classCnt += isset( $data['aopt12']) ? $data['aopt12'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt13']) ? $data['aopt13'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt14']) ? $data['aopt14'] * 0.15 : 0 ;
		$classCnt += isset( $data['aopt15']) ? $data['aopt15'] * 0.2 : 0 ;
		$area3    = $classCnt ;
		$retTotal += $area3 * 0.25 ;
		$classCnt = 0 ;

		$classCnt += isset( $data['aopt16']) ? $data['aopt16'] * 0.5 : 0 ;
		$classCnt += isset( $data['aopt17']) ? $data['aopt17'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt18']) ? $data['aopt18'] * 0.25 : 0 ;
		$area4    = $classCnt ;
		$retTotal += $area4 * 0.2 ;
		$classCnt = 0 ;

		$classCnt += isset( $data['aopt19']) ? $data['aopt19'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt20']) ? $data['aopt20'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt21']) ? $data['aopt21'] * 0.25 : 0 ;
		$classCnt += isset( $data['aopt22']) ? $data['aopt22'] * 0.25 : 0 ;
		$area5    = $classCnt;
		$retTotal += $area5 * 0.2;
		$classCnt = 0 ;

		// dump( $retTotal) ;
		return array( $retTotal, $area1, $area2, $area3, $area4, $area5) ;
	}

	// add by Angus 2017.07.09
	protected function getRequest( $method, $index) {
		switch ( $method) {
			case 'get':
				return isset( $this->request->get[$index]) ? $this->request->get[$index] : null ;
				break ;
			case 'post':
				return isset( $this->request->post[$index]) ? $this->request->post[$index] : null ;
				break ;
			default :
				return "";
				break ;
		}
	}

	/**
	 * [rankBuild 名次序]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-05-02
	 */
	protected function rankBuild( $data = array()) {
		// dump( $data) ;
		// 分數排序
		$scoreArr = array() ;
		foreach ($data as $planID => $score) {
			$scoreArr[$score][] = $planID ;
		}
		ksort( $scoreArr) ;
		// dump( $scoreArr) ;

		// 名次排序
		$rankIdx = 1 ;
		foreach ($scoreArr as $score => $tmpArr) {
			sort( $tmpArr) ;
			$rank = $rankIdx ;
			foreach ($tmpArr as $iCnt => $planID) {
				$rankArr[$planID] = $rank ;
				$rankIdx ++ ;
			}
		}

		// dump( $rankArr) ;
		return $rankArr ;
	}


	protected function getAllPlanScore( $pid = "") {
		// dump( $pid) ;
		$judgeScore = $this->model_bike_judge->getJudgeScore2() ;
		$planCnt         = 0 ;
		$judgeCnt        = count( $judgeScore) ;
		$planIDScore     = array() ;
		$planScore       = array() ;
		$allPlanScoreAVG = array() ;
		$Plan18ScoreAVG  = array() ;

		/**
		 * [$judgeID 計劃排名]
		 * @var [type]
		 */
		foreach ( $judgeScore as $judgeID => $scoreArr) {
			$rank = 1 ;
			foreach ( $scoreArr as $planID => $score) {
				if ( !isset( $ordinal[$planID])) {
					$ordinal[$planID] = 0 ;
				}
				if ( $rank <=5) {
					$ordinal[$planID] += 1 ;
				} else if ($rank >=6 && $rank <= 10) {
					$ordinal[$planID] += 2 ;
				} else if ($rank >=11 && $rank <= 15) {
					$ordinal[$planID] += 3 ;
				} else if ($rank >=16 && $rank <= 20) {
					$ordinal[$planID] += 4 ;
				} else if ($rank >=21 && $rank <= 25) {
					$ordinal[$planID] += 5 ;
				} else if ($rank >=26 && $rank <= 30) {
					$ordinal[$planID] += 6 ;
				} else if ($rank >=31 && $rank <= 35) {
					$ordinal[$planID] += 7 ;
				} else if ($rank >=36 && $rank <= 40) {
					$ordinal[$planID] += 8 ;
				} else if ($rank >=41 && $rank <= 45) {
					$ordinal[$planID] += 9 ;
				} else if ($rank >=46 && $rank <= 50) {
					$ordinal[$planID] += 10 ;
				}
				$rank ++ ;
			}
		}

		$ordinal = $this->rankBuild( $ordinal) ;
		// end 計劃排名

		foreach ($judgeScore as $judgeID => $planList) {
			if ( $planCnt < count( $planList)) $planCnt = count( $planList) ;

			foreach ($planList as $planID => $planRow) {
				$encodeData = json_decode( $planRow['answer'], true) ;
				// dump( $encodeData) ;
				$retTotal = $this->totalCnt( $encodeData) ;

				// $judgeScore[$judgeID][$planID]['a1'] = $retTotal[1] ;
				// $judgeScore[$judgeID][$planID]['a2'] = $retTotal[2] ;
				// $judgeScore[$judgeID][$planID]['a3'] = $retTotal[3] ;
				// $judgeScore[$judgeID][$planID]['a4'] = $retTotal[4] ;
				// $judgeScore[$judgeID][$planID]['a5'] = $retTotal[5] ;


				if ( !isset($planScore[$planID]['a1'])) $planScore[$planID]['a1'] = 0 ;
				if ( !isset($planScore[$planID]['a2'])) $planScore[$planID]['a2'] = 0 ;
				if ( !isset($planScore[$planID]['a3'])) $planScore[$planID]['a3'] = 0 ;
				if ( !isset($planScore[$planID]['a4'])) $planScore[$planID]['a4'] = 0 ;
				if ( !isset($planScore[$planID]['a5'])) $planScore[$planID]['a5'] = 0 ;
				$planScore[$planID]['a1'] += $retTotal[1] ;
				$planScore[$planID]['a2'] += $retTotal[2] ;
				$planScore[$planID]['a3'] += $retTotal[3] ;
				$planScore[$planID]['a4'] += $retTotal[4] ;
				$planScore[$planID]['a5'] += $retTotal[5] ;

				if ( !isset($allPlanScore['a1'])) $allPlanScore['a1'] = 0 ;
				if ( !isset($allPlanScore['a2'])) $allPlanScore['a2'] = 0 ;
				if ( !isset($allPlanScore['a3'])) $allPlanScore['a3'] = 0 ;
				if ( !isset($allPlanScore['a4'])) $allPlanScore['a4'] = 0 ;
				if ( !isset($allPlanScore['a5'])) $allPlanScore['a5'] = 0 ;
				$allPlanScore['a1'] += $retTotal[1] ;
				$allPlanScore['a2'] += $retTotal[2] ;
				$allPlanScore['a3'] += $retTotal[3] ;
				$allPlanScore['a4'] += $retTotal[4] ;
				$allPlanScore['a5'] += $retTotal[5] ;


				if ( !isset($Plan18ScoreAVG['a1'])) $Plan18ScoreAVG['a1'] = 0 ;
				if ( !isset($Plan18ScoreAVG['a2'])) $Plan18ScoreAVG['a2'] = 0 ;
				if ( !isset($Plan18ScoreAVG['a3'])) $Plan18ScoreAVG['a3'] = 0 ;
				if ( !isset($Plan18ScoreAVG['a4'])) $Plan18ScoreAVG['a4'] = 0 ;
				if ( !isset($Plan18ScoreAVG['a5'])) $Plan18ScoreAVG['a5'] = 0 ;
				if ( $ordinal[$planID] <= 18) {
					$Plan18ScoreAVG['a1'] += $retTotal[1] ;
					$Plan18ScoreAVG['a2'] += $retTotal[2] ;
					$Plan18ScoreAVG['a3'] += $retTotal[3] ;
					$Plan18ScoreAVG['a4'] += $retTotal[4] ;
					$Plan18ScoreAVG['a5'] += $retTotal[5] ;
				}
			}
		}

		/**
		 * [$key 全場計劃平均分]
		 * @var [type]
		 */
		foreach ($allPlanScore as $key => $value) {
			$allPlanScoreAVG[$key] = round( $value / $planCnt / $judgeCnt, 2) ;
		}

		foreach ($Plan18ScoreAVG as $key => $value) {
			$Plan18ScoreAVG[$key] = round( $value / 18 / $judgeCnt, 2) ;
		}

		/**
		 * [$key 單場計劃平均分]
		 * @var [type]
		 */
		foreach ($planScore[$pid] as $key => $value) {
			$planIDScore[$key] = round( $value / $judgeCnt, 2) ;
		}


		// dump( $planIDScore) ;
		// dump( $allPlanScore) ;
		// dump( $planScore) ;
		// dump( $judgeScore) ;
		return array( $allPlanScoreAVG, $Plan18ScoreAVG, $planIDScore) ;
	}

	protected  function biludJsString ( $data) {
		// dump( $data) ;
		$jsStr = "{name: '47條路線總平均', data: [".$data[0]['a1'].",".$data[0]['a2'].",".$data[0]['a3'].",".$data[0]['a4'].",".$data[0]['a5']."], pointPlacement: 'on'}" ;
		$jsStr .= ",{name: '18條潛力案件平均', data: [".$data[1]['a1'].",".$data[1]['a2'].",".$data[1]['a3'].",".$data[1]['a4'].",".$data[1]['a5']."], pointPlacement: 'on'}" ;
		$jsStr .= ",{name: '本計畫路線評分', data: [".$data[2]['a1'].",".$data[2]['a2'].",".$data[2]['a3'].",".$data[2]['a4'].",".$data[2]['a5']."], pointPlacement: 'on'}" ;
		return $jsStr ;
	}
}