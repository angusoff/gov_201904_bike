<?php
class ControllerBikePlanview extends Controller {
	private $error         = array() ;
	private $data          = array() ;
	private $showSelfCheck = false ;

	public function index() {
		$this->load->model( 'bike/report') ;
		$this->common_info() ;
		$data = $this->data ;

		$data['planList'] = $this->model_bike_report->getPlanLists() ;
		// dump( $data['planList']) ;
		$planInfo = $this->model_bike_report->getPlanAnsStatus() ;

		// dump( $planInfo) ;
		foreach ( $data['planList'] as $iCnt => $tmp) {
			if ( isset( $planInfo[$tmp['idx']])) {
				$data['planList'][$iCnt]['evaluation_mData'] = isset( $planInfo[$tmp['idx']]['evaluation_mData']) ? $planInfo[$tmp['idx']]['evaluation_mData'] : "" ;
				$data['planList'][$iCnt]['checking_mData'] = isset( $planInfo[$tmp['idx']]['checking_mData']) ? $planInfo[$tmp['idx']]['checking_mData'] : "" ;
				// dump( $tmp) ;
				if ( !empty( $tmp['organ'])) $data['planList'][$iCnt]['evaluation_mData'] = "V<br>" . $data['planList'][$iCnt]['evaluation_mData'] ;
				if ( !empty( $tmp['self_time'])) $data['planList'][$iCnt]['checking_mData'] = "V<br>" . $data['planList'][$iCnt]['checking_mData'] ;

				if ( !empty( $data['planList'][$iCnt]['evaluation_mData'])) {
					$url = $this->url->link('bike/planview/view1', "pid={$tmp['idx']}&doc_num={$tmp['doc_num']}", true) ;
					$data['planList'][$iCnt]['evaluation_mData'] .= '<a class="btn btn-secondary" target="_blank" href="'. $url . '"><i class="fa fa-search fa-fw"></i></a>' ;
				}
				if ( !empty( $data['planList'][$iCnt]['checking_mData'])) {
					$url = $this->url->link('bike/planview/view2', "pid={$tmp['idx']}&doc_num={$tmp['doc_num']}", true) ;
					$url3 = $this->url->link('bike/planview/view3', "pid={$tmp['idx']}&doc_num={$tmp['doc_num']}", true) ;
					$data['planList'][$iCnt]['checking_mData'] .= '<a class="btn btn-secondary" target="_blank" href="'. $url . '"><i class="fa fa-search fa-fw"></i></a><a class="btn btn-secondary" target="_blank" href="'. $url3 . '"><i class="fa fa-search fa-fw"></i></a>' ;
				}



			} else {
				$data['planList'][$iCnt]['evaluation_mData'] = "" ;
				$data['planList'][$iCnt]['checking_mData'] = "" ;
			}
		}
		// dump( $data['planList']) ;

		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('bike/planview', $data));
	}

	public function view1 () {
		$this->load->model( 'bike/report') ;
		$this->common_info() ;
		$filter_data = array (
			"idx"     => $this->request->get['pid'],
			"doc_num" => $this->request->get['doc_num'],
		) ;
		$data['planRow'] = $this->model_bike_report->getPlanName( $filter_data) ;
		$isReport = $this->model_bike_report->getAnswer( $filter_data) ;
		// dump( $isReport) ;

		$items = json_decode( $isReport->row['answer']) ;
		foreach ($items as $keyName => $value) {
			$data[$keyName] = isset( $value) ? $value : "" ;
		}
		$data['startRoute']  = $isReport->row['startRoute'] ;
		$data['plan']        = $isReport->row['plan'] ;
		$data['planDate1']   = $isReport->row['planDate1'] ;
		$data['planDate2']   = $isReport->row['planDate2'] ;
		$data['planDate3']   = $isReport->row['planDate3'] ;
		$data['planDate4']   = $isReport->row['planDate4'] ;

		$data['planStatus']  = $isReport->row['planStatus'] ;
		$data['environment'] = $isReport->row['environment'] ;
		$data['route']       = $isReport->row['route'] ;

		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('bike/view_evaluationForm', $data));
	}

	public function view2 () {
		$this->load->model( 'bike/report') ;
		$this->common_info() ;
		$filter_data = array (
			"idx"     => $this->request->get['pid'],
			"doc_num" => $this->request->get['doc_num'],
		) ;
		$data['planRow'] = $this->model_bike_report->getPlanName( $filter_data) ;
		$isReport = $this->model_bike_report->getSelfAnswer( $filter_data) ;
		$items = json_decode( $isReport->row['answer']) ;
		foreach ($items as $keyName => $value) {
			$data[$keyName] = isset( $value) ? $value : "" ;
		}


		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('bike/view_checkForm', $data));
	}

	public function view3 () {
		$this->load->model( 'bike/report') ;
		$this->common_info() ;
		$filter_data = array (
			"idx"     => $this->request->get['pid'],
			"doc_num" => $this->request->get['doc_num'],
		) ;
		$data['planRow'] = $this->model_bike_report->getPlanName( $filter_data) ;
		$isReport = $this->model_bike_report->getSelfAnswerOldVersion( $filter_data) ;
		// dump( $data['planRow']) ;
		$items = json_decode( $isReport->row['answer']) ;
		foreach ($items as $keyName => $value) {
			$data[$keyName] = isset( $value) ? $value : "" ;
		}
		$data['modify_date'] = $isReport->row['modify_date'] ;


		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('bike/view_checkForm2', $data));
	}







	private function common_info() {

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			// 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			'href' => $this->url->link('common/dashboard', 'token=', true)
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			// 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			'href' => $this->url->link('common/dashboard', 'token=', true)
		);

		// 訊息
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'] ;
			foreach ( $this->error as $errKey => $tmpStr) {
				if ( $errKey != "warning") $this->data['error_warning'] .= "<br>{$tmpStr}" ;
			}
		} else {
			$this->data['error_warning'] = '' ;
		}
		if ( isset( $this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'] ;
			unset( $this->session->data['success']) ;
		} else {
			$this->data['success'] = '' ;
		}
	}
}