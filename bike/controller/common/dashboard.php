<?php
class ControllerCommonDashboard extends Controller {
	private $error = array() ;
	private $showSelfCheck = false ;

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-03-30
update tb_plan_list set organ=null,
name=null,
tel=null,
mobile=null,
mail = null,
self_time = null
	 */
	public function index() {
		// dump( "ControllerCommonDashboard") ;
		// exit() ;
		// dump( $this->session->data) ;

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			// 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			'href' => $this->url->link('common/dashboard', 'token=', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			// 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			'href' => $this->url->link('common/dashboard', 'token=', true)
		);

		$this->load->model( 'bike/report') ;
		$planListArr = $this->model_bike_report->getPlanLists() ;

		// $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)

		foreach ($planListArr as $iCnt => $tmp) {
			if ( isset( $this->session->data['dn']) && isset( $this->session->data['doc_num']) && $this->session->data['doc_num'] == $tmp['doc_num']) {
				$tmp['href'] = $this->url->link('common/dashboard/logout', 'dn=' . $tmp['idx'], true) ;
				$tmp['btName'] = "登出" ;
				$tmp['btCss'] = "btn-success" ;
			} else {
				$tmp['href'] = $this->url->link('common/dashboard/conformDocNum', 'dn=' . $tmp['idx'], true) ;
				$tmp['btName'] = "登入" ;
				$tmp['btCss'] = "btn-primary" ;
			}
			$planListArr[$iCnt] = $tmp ;
		}
		$data['planList'] = $planListArr ;
		// dump( $planListArr) ;


		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/dashboard', $data));
	}

	/**
	 * [logout description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-02
	 */
	public function logout() {
		unset( $this->session->data['dn']) ;
		unset( $this->session->data['doc_num']) ;
		$this->response->redirect($this->url->link('common/dashboard', "", true)) ;
	}

	/**
	 * [conformDocNum description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-03-30
	 */
	public function conformDocNum() {
		$this->load->model( 'bike/report') ;

		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateDocNumForm()) {
			$this->session->data['doc_num'] = $this->request->post['item1'] ;
			$this->response->redirect($this->url->link('common/dashboard/self_evaluationForm', "", true)) ;
		}

		// 訊息
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
			foreach ( $this->error as $errKey => $tmpStr) {
				if ( $errKey != "warning") $data['error_warning'] .= "<br>{$tmpStr}" ;
			}
		} else {
			$data['error_warning'] = '' ;
		}

		$filter_data = array (
			"idx" => $this->request->get['dn']
		) ;
		$this->session->data['dn'] = $this->request->get['dn'] ;
		$data['planRow']           = $this->model_bike_report->getPlanName( $filter_data) ;
		$data['actionStr']         = $this->url->link('common/dashboard/conformDocNum', 'dn=' . $this->request->get['dn'], true) ;

		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('bike/conform', $data));
	}

	/**
	 * [self_evaluationForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-03-31
	 */
	public function self_evaluationForm() {
		$this->load->model( 'bike/report') ;
		// dump( $this->request->server['REQUEST_METHOD']) ;
		// dump( $this->request->post) ;
		// exit() ;

		$method = isset( $this->request->post['save_submit']) ? $this->request->post['save_submit'] : "" ;
		// if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $method == "save") {
		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $method == "submit") {
			// dump( $this->request->post) ;
			$insArr = array(
				"pid"         => $this->session->data['dn'],
				"doc_num"     => $this->session->data['doc_num'],
				"organ"       => $this->request->post['contact_organ'],
				"name"        => $this->request->post['contact_name'],
				"tel"         => $this->request->post['contact_tel'],
				"mobile"      => $this->request->post['contact_mobile'],
				"mail"        => $this->request->post['contact_mail'],

				"startRoute"  => $this->request->post['startRoute'],
				"plan"        => $this->request->post['plan'],
				"planDate1"   => $this->request->post['planDate1'],
				"planDate2"   => $this->request->post['planDate2'],
				"planDate3"   => $this->request->post['planDate3'],
				"planDate4"   => $this->request->post['planDate4'],
				"planStatus"  => $this->request->post['planStatus'],
				"environment" => $this->request->post['environment'],
				"route"       => $this->request->post['route'],

				"save_submit" => $method
			) ;
			$answerArr = array() ;
			for ($i=1; $i < 16; $i++) {
				if ( $i != 3) {
					$keyName = "item".$i ;
					$answerArr[$keyName] = isset( $this->request->post[$keyName]) ? urlencode($this->request->post[$keyName]) : "" ;
				}
			}
			$insArr['answer'] = $answerArr ;
			$this->model_bike_report->saveAnswer( $insArr) ;
			$this->model_bike_report->updatePlan( $insArr) ;

			$this->session->data['success'] = "填報資料 已送出";
		} else if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $method == "") {
			// dump( $this->request->post) ;
			$insArr = array(
				"pid"         => $this->session->data['dn'],
				"doc_num"     => $this->session->data['doc_num'],

				"startRoute"  => $this->request->post['startRoute'],
				"plan"        => $this->request->post['plan'],
				"planDate1"   => $this->request->post['planDate1'],
				"planDate2"   => $this->request->post['planDate2'],
				"planDate3"   => $this->request->post['planDate3'],
				"planDate4"   => $this->request->post['planDate4'],
				"planStatus"  => $this->request->post['planStatus'],
				"environment" => $this->request->post['environment'],
				"route"       => $this->request->post['route'],

				"save_submit" => $method
			) ;
			$answerArr = array() ;
			for ($i=1; $i < 16; $i++) {
				if ( $i != 3) {
					$keyName = "item".$i ;
					$answerArr[$keyName] = isset( $this->request->post[$keyName]) ? urlencode($this->request->post[$keyName]) : "" ;
				}
			}
			$insArr['answer'] = $answerArr ;
			$this->model_bike_report->saveAnswer( $insArr) ;

			$this->session->data['success'] = "填報資料 儲存成功";
		}


		$filter_data = array (
			"idx"     => $this->session->data['dn'],
			"doc_num" => $this->session->data['doc_num'],
		) ;
		$data['planRow'] = $this->model_bike_report->getPlanName( $filter_data) ;
		if ( trim( $data['planRow']['organ']) != "") {
			$data['showButton'] = "" ;
		} else {
			$data['showButton'] = "show" ;
		}

		// 訊息
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
			foreach ( $this->error as $errKey => $tmpStr) {
				if ( $errKey != "warning") $data['error_warning'] .= "<br>{$tmpStr}" ;
			}
		} else {
			$data['error_warning'] = '' ;
		}
		if ( isset( $this->session->data['success'])) {
			$data['success'] = $this->session->data['success'] ;
			unset( $this->session->data['success']) ;
		} else {
			$data['success'] = '' ;
		}

		// 檢查是否有填報過
		$isReport = $this->model_bike_report->getAnswer( $filter_data) ;
		if ( $isReport->num_rows == 0 ) {
			// 填報內容
			for ($i=1; $i < 16; $i++) {
				if ( $i != 3) {
					$keyName = "item".$i ;
					$data[$keyName] = isset( $this->request->post[$keyName]) ? $this->request->post[$keyName] : "" ;
				}
			}

			$data['startRoute']  = isset( $this->request->post['startRoute']) ? $this->request->post['startRoute'] : "" ;
			$data['plan']        = isset( $this->request->post['plan']) ? $this->request->post['plan'] : "" ;
			$data['planDate1']   = isset( $this->request->post['planDate1']) ? $this->request->post['planDate1'] : "" ;
			$data['planDate2']   = isset( $this->request->post['planDate2']) ? $this->request->post['planDate2'] : "" ;
			$data['planDate3']   = isset( $this->request->post['planDate3']) ? $this->request->post['planDate3'] : "" ;
			$data['planDate4']   = isset( $this->request->post['planDate4']) ? $this->request->post['planDate4'] : "" ;

			$data['planStatus']  = isset( $this->request->post['planStatus']) ? $this->request->post['planStatus'] : "" ;
			$data['environment'] = isset( $this->request->post['environment']) ? $this->request->post['environment'] : "" ;
			$data['route']       = isset( $this->request->post['route']) ? $this->request->post['route'] : "" ;
		} else {
			// dump( $isReport->row['answer']) ;
			// dump( $isReport->row) ;
			$items = json_decode( $isReport->row['answer']) ;
			foreach ($items as $keyName => $value) {
				$data[$keyName] = isset( $value) ? $value : "" ;
			}
			$data['startRoute']  = $isReport->row['startRoute'] ;
			$data['plan']        = $isReport->row['plan'] ;
			$data['planDate1']   = $isReport->row['planDate1'] ;
			$data['planDate2']   = $isReport->row['planDate2'] ;
			$data['planDate3']   = $isReport->row['planDate3'] ;
			$data['planDate4']   = $isReport->row['planDate4'] ;

			$data['planStatus']  = $isReport->row['planStatus'] ;
			$data['environment'] = $isReport->row['environment'] ;
			$data['route']       = $isReport->row['route'] ;

		}

		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('bike/self_evaluationForm', $data));
	}

	/**
	 * [self_checkForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-01
	 */
	public function self_checkForm() {
		$this->load->model( 'bike/report') ;
		// dump( $this->request->post) ;
		// exit() ;

		$method = isset( $this->request->post['save_submit']) ? $this->request->post['save_submit'] : "" ;
		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $method == "submit") {


			// dump( $this->request->post) ;
			$insArr = array(
				"pid"         => $this->session->data['dn'],
				"doc_num"     => $this->session->data['doc_num'],
				"save_submit" => $method
			) ;
			$answerArr = array() ;
			for ($i=1; $i < 23; $i++) {
				$keyName = "opt".$i ;
				$answerArr[$keyName] = isset( $this->request->post[$keyName]) ? $this->request->post[$keyName] : "" ;
				if ( $answerArr[$keyName] == "") $this->error['warning'] = "問卷填寫不完全" ;
			}

			if ( empty( $this->error['warning'])) {
				$insArr['answer'] = $answerArr ;
				$insArr['acc1-1'] = isset($this->request->post['acc1-1']) ? $this->request->post['acc1-1'] : "" ;
				$insArr['acc1-5'] = isset($this->request->post['acc1-5']) ? $this->request->post['acc1-5'] : "" ;
				$insArr['acc2-2'] = isset($this->request->post['acc2-2']) ? $this->request->post['acc2-2'] : "" ;
				$insArr['acc3-2'] = isset($this->request->post['acc3-2']) ? $this->request->post['acc3-2'] : "" ;
				$insArr['acc3-3'] = isset($this->request->post['acc3-3']) ? $this->request->post['acc3-3'] : "" ;

				$this->model_bike_report->saveSelfAnswer( $insArr) ;
				$this->model_bike_report->updatePlanSelf( $insArr) ;

				$this->session->data['success'] = "填報資料 已送出";
			}
		} else if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $method == "") {
			$insArr = array(
				"pid"         => $this->session->data['dn'],
				"doc_num"     => $this->session->data['doc_num'],
				"save_submit" => $method
			) ;
			$answerArr = array() ;
			for ($i=1; $i < 23; $i++) {
				$keyName = "opt".$i ;
				$answerArr[$keyName] = isset( $this->request->post[$keyName]) ? $this->request->post[$keyName] : "" ;
			}

			$insArr['answer'] = $answerArr ;
				$insArr['acc1-1'] = isset($this->request->post['acc1-1']) ? $this->request->post['acc1-1'] : "" ;
				$insArr['acc1-5'] = isset($this->request->post['acc1-5']) ? $this->request->post['acc1-5'] : "" ;
				$insArr['acc2-2'] = isset($this->request->post['acc2-2']) ? $this->request->post['acc2-2'] : "" ;
				$insArr['acc3-2'] = isset($this->request->post['acc3-2']) ? $this->request->post['acc3-2'] : "" ;
				$insArr['acc3-3'] = isset($this->request->post['acc3-3']) ? $this->request->post['acc3-3'] : "" ;
			$this->model_bike_report->saveSelfAnswer( $insArr) ;

			$this->session->data['success'] = "填報資料 儲存成功";
		}

		// 訊息
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
			foreach ( $this->error as $errKey => $tmpStr) {
				if ( $errKey != "warning") $data['error_warning'] .= "<br>{$tmpStr}" ;
			}
		} else {
			$data['error_warning'] = '' ;
		}
		if ( isset( $this->session->data['success'])) {
			$data['success'] = $this->session->data['success'] ;
			unset( $this->session->data['success']) ;
		} else {
			$data['success'] = '' ;
		}

		$filter_data = array (
			"idx"     => $this->session->data['dn'],
			"doc_num" => $this->session->data['doc_num'],
		) ;
		$data['planRow'] = $this->model_bike_report->getPlanName( $filter_data) ;
		if ( trim( $data['planRow']['self_time']) != "") {
			$data['showButton'] = "" ;
		} else {
			$data['showButton'] = "show" ;
		}

		// 檢查是否有填報過
		$isReport = $this->model_bike_report->getSelfAnswer( $filter_data) ;
		// dump( $isReport) ;
		if ( $isReport->num_rows == 0 ) {
			// 填報內容
			for ($i=1; $i < 23; $i++) {
				$keyName = "opt".$i ;
				$data[$keyName] = isset( $this->request->post[$keyName]) ? $this->request->post[$keyName] : "" ;
			}
			$data['acc1_1'] = isset( $this->request->post['acc1-1']) ? $this->request->post['acc1-1'] : "" ;
			$data['acc1_1'] = isset( $this->request->post['acc1-1']) ? $this->request->post['acc1-1'] : "" ;
			$data['acc1_1'] = isset( $this->request->post['acc1-1']) ? $this->request->post['acc1-1'] : "" ;
			$data['acc1_1'] = isset( $this->request->post['acc1-1']) ? $this->request->post['acc1-1'] : "" ;
			$data['acc1_1'] = isset( $this->request->post['acc1-1']) ? $this->request->post['acc1-1'] : "" ;
		} else {
			$items = json_decode( $isReport->row['answer']) ;
			foreach ($items as $keyName => $value) {
				$data[$keyName] = isset( $value) ? $value : "" ;
			}
			$data['acc1_1'] = isset( $isReport->row['acc1_1']) ? $isReport->row['acc1_1'] : "" ;
			$data['acc1_5'] = isset( $isReport->row['acc1_5']) ? $isReport->row['acc1_5'] : "" ;
			$data['acc2_2'] = isset( $isReport->row['acc2_2']) ? $isReport->row['acc2_2'] : "" ;
			$data['acc3_2'] = isset( $isReport->row['acc3_2']) ? $isReport->row['acc3_2'] : "" ;
			$data['acc3_3'] = isset( $isReport->row['acc3_3']) ? $isReport->row['acc3_3'] : "" ;
		}


		$qList = $this->get_self_check_questionnaire() ;
		$data['qList'] = $qList ;

		$data['header']         = $this->load->controller('common/header');
		$data['column_left']    = $this->load->controller('common/column_left');
		$data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('bike/self_checkForm2', $data));
	}

	/**
	 * [validateDocNumForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-03-30
	 */
	protected function validateDocNumForm() {

		if ( $this->session->data['dn'] != $this->request->get['dn']) {
			$this->error['warning1'] = "索引碼錯誤" ;
		}
		if ( !empty( $this->request->post['item1'])) {
			$filter_data = array(
				"idx"     => $this->session->data['dn'],
				"doc_num" => $this->request->post['item1']
			) ;

			$plan = $this->model_bike_report->checkPlan( $filter_data) ;
			if ( $plan->num_rows == 0) {
				$this->error['doc_num'] = "核定文號錯誤" ;
			} else {
				if ( $plan->row['organ'] != "") {
					$this->showSelfCheck = true ;
				}
			}

		} else {
			$this->error['doc_num'] = "核定文號錯誤" ;
		}
		if ( count( $this->error)) {
			$this->error['warning'] = "輸入錯誤" ;
		}

		return !$this->error;
	}

	/**
	 * [get_self_check_questionnaire description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-04-02
	 */
	protected function get_self_check_questionnaire() {
		$qListArr = array(
			"1.起迄點標示是否明確",
			"2.是否有自行車租賃站(點)",
			"3.沿線或鄰近便利商店及補給站",
			"4.起終點是否具公共運輸無縫接軌",
			"5.周邊是否具車輛停車場",
			"6.沿線每5公里是否有流廁及無障礙廁所",
			"7.沿線是否具2座以上導覽說明牌",
			"8.車道路面是否平整",
			"9.全線自行車專用道標誌/標線",
			"10.每5公里是否具休憩點設施",
			"11.休憩點設施或流廁周邊是否有自行車停車架",
			"12.全線告示/指示牌是否明確: 公里牌、方向牌、指示牌…等",
			"13.是否具遊客中心或巡邏站",
			"14.設施維護 : 護欄、標示牌面、休憩站桌椅、照明、邊坡",
			"15.沿線或鄰近1公里內具在地文化或特色景點",
			"16.沿線具景觀建物或打卡標地",
			"17.特色風景 : 如夕陽、湖泊、田園、季節性風景",
			"18.自然生態環境 : 濕地、森林步道、生態公園",
			"19.曾獲媒體報導",
			"20.沿線或臨近具在地美食及商家",
			"21.與其他縣市自行車道可串接性",
		) ;

		return $qListArr ;
	}















}