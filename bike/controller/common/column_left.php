<?php
class ControllerCommonColumnLeft extends Controller {
	public function index() {
        // dump( $this->session->data) ;
        //左邊選單預設打開
        $data["col_left"] = 'active';
        if(isset($this->session->data['colLeft'])){
            $data["col_left"] = $this->session->data['colLeft'] ;
        }

        $data['loginHandle'] = true ;
        $data['loginJudge']  = true ;
        $data['showHandle']  = false ;
        $data['showJudge']   = false ;

        /**
         * 承辨登入
         */
        if ( isset( $this->session->data['dn']) && isset( $this->session->data['doc_num'])) {
			$data['loginJudge'] = false ;
			$data['showHandle'] = true ;
        }

        /**
         * 評委登入
         */
        if ( isset( $this->session->data['judgeID']) && isset( $this->session->data['judgeName'])) {
			$data['loginName']   = $this->session->data['judgeName'] ;
			$data['loginHandle'] = false ;
			$data['showJudge']   = true ;
        }
        return $this->load->view('common/column_left', $data);
	}
}
